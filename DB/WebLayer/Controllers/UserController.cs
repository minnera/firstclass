﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebLayer.Classes;

namespace WebLayer.Controllers
{
    public class UserController : Controller
    {
        // GET: User
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Login()
        {
            //TODO check username and pass
            Session["UserID"] = 42;

            //ha módosítást végzek el akkor mindig visszaugrok
            return RedirectToAction("Index");
        }

        public ActionResult Logout()
        {
            Session.Abandon();
            return RedirectToAction("Index");
        }
        [MyAuthorizeFilter]
        public ActionResult Secret()
        {
            //az adatlopás
            return Content("SUPER SECRET CONTENT");
        }
    }
}