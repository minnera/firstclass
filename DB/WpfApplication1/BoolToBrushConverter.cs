﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Media;

namespace WpfApplication1
{
    class BoolToBrushConverter : IValueConverter
    {
        /// <summary>
        /// CM -> UI
        /// </summary>
        /// <param name="value">VM property [BOOL]</param>
        /// <param name="targetType">UI property type</param>
        /// <param name="parameter">XAML ConverterParameter</param>
        /// <param name="culture">Thread culture</param>
        /// <returns>UI value [BRUSH]</returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            bool flag = (bool)value;
            return flag ? Brushes.GreenYellow : Brushes.Crimson;
        }

        /// <summary>
        /// UI -> VM
        /// </summary>
        /// <param name="value">UI property</param>
        /// <param name="targetType">VM property type</param>
        /// <param name="parameter">XAML ConverterParameter</param>
        /// <param name="culture">Thread culture</param>
        /// <returns>VM value</returns>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            //arra készülünk, hogy ez nem fog meghívódni
            throw new NotImplementedException();
        }
    }
}
