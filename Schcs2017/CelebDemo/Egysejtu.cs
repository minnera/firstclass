﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CelebDemo
{
    delegate void EgysejtuDelegate(Egysejtu a);
    class Egysejtu :ILegzokepes
    {
        private int _meret = 1;
        public event EgysejtuDelegate MeretValtozott;
        public int Meret
        {
            get
            {
                return _meret;
            }

            set
            {
                _meret = value;
                //ide eseményt
                if(MeretValtozott != null)
                    MeretValtozott(this);
            }
        }

        public void Lelegzik()
        {
            Console.WriteLine("Piheg");
        }

        public static Egysejtu operator +(Egysejtu e1, Egysejtu e2)
        {
            Egysejtu result = new Egysejtu();
            result.Meret = e1.Meret + e2.Meret;
            return result;
        }
        public static explicit operator Celeb(Egysejtu e)
        {
            Celeb result = new ButaCeleb("Egysejtű");
            result.Iq = e.Meret;
            return result;
        }
        
    }
}
