﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace wpf4_2_android
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        static Random R = new Random();
        private string mesterPassword = "01258";
        private string currentPassword = String.Empty;
        private bool platformActive = false;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void MainWindow_OnLoaded(object sender, RoutedEventArgs e)
        {
            Grid myGrid = this.Content as Grid;
            SizeToContent = SizeToContent.WidthAndHeight;
            myGrid.Width = 300;
            myGrid.Height = 300;

            for (int y = 0; y < 3; y++)
            {
                myGrid.RowDefinitions.Add(new RowDefinition());
                myGrid.ColumnDefinitions.Add(new ColumnDefinition());
                for (int x = 0; x < 3; x++)
                {
                    Label newlabel = new Label();
                    newlabel.Content = (y*3 + x).ToString();
                    Grid.SetColumn(newlabel, x);
                    Grid.SetRow(newlabel, y);
                    newlabel.BorderBrush = Brushes.Black;
                    newlabel.BorderThickness = new Thickness(3);
                    newlabel.Background = Brushes.Khaki;
                    newlabel.Margin = new Thickness(10);
                    newlabel.FontSize = 50;
                    newlabel.HorizontalContentAlignment = HorizontalAlignment.Center;
                    newlabel.VerticalContentAlignment = VerticalAlignment.Center;
                    newlabel.MouseMove += Newlabel_MouseMove; //itt +=tabulátor enterrel hoztuk ezt létre
                    myGrid.Children.Add(newlabel);
                }
            }
            myGrid.Background = Brushes.Black;
        }

        // Direct: xxxChanged (SelectedItemChanged), Click
        // Bubbling: Control => parent => root, RoutedEvent (MouseMove, MouseDown, MouseUp, KeyDown,..)
        // Tunneling: root => child => control, Preview Event ( PreviewMouseDown, ....)
        // e.Handled = true => STOP PROPAGATION (az esemény tovább dobását tudod így megállíani)
        // sender => method caller
        // e.Source => event source (logical)
        // e.OriginalSource => event source (visual)

        private void Newlabel_MouseMove(object sender, MouseEventArgs e)
        {
            (sender as Label).BorderBrush = new SolidColorBrush(Color.FromRgb(
                (byte)R.Next(256), (byte)R.Next(256), (byte)R.Next(256)
                ));
        }

        private void UIElement_OnMouseDown(object sender, MouseButtonEventArgs e)
        {
            //mostantól kezdve aktí@ a jelszó bekérés
            platformActive = true;
            currentPassword = String.Empty;
        }

        private void UIElement_OnMouseMove(object sender, MouseEventArgs e)
        {
            //Title = e.GetPosition(this).ToString();
            if (platformActive && e.Source is Label)
            {
                //csak akkor kell hozzáadni a számot ha az a szám az más mint az előző
                Label l = e.Source as Label;
                char c = l.Content.ToString()[0];
                if (currentPassword.Length == 0 || c != currentPassword[currentPassword.Length - 1])
                {
                    currentPassword += c;
                    l.Background = Brushes.YellowGreen;
                    Title = currentPassword;
                }
            }
        }

        private void UIElement_OnMouseUp(object sender, MouseButtonEventArgs e)
        {
            platformActive = false;
            foreach (UIElement v in (sender as Grid).Children)
            {
                if (v is Label)
                {
                    (v as Label).Background = Brushes.Khaki;
                    (v as Label).BorderBrush = Brushes.Black;
                    //staticresource => app.xaml
                }
            }
            if (currentPassword == mesterPassword)
            {
                MessageBox.Show("Passed");
                //TODO 
                //HomeWindow (alkalmazások listája, listbox + combobox ; + DEL button)
                //HomeWindowVM (apps, apptoexecute, apptodelete)
                //MobileApp (name, creation date, author, size)
                // listbox duplaklikk => Futtatás
                // Button katt => Listából eltávolítás kérdés után 
                HomeWindow win = new HomeWindow();
                win.ShowDialog();
            }
            else
            {
                MessageBox.Show("Failed");
            }
        }
    }
}
//jelszóhoz mátrix
//0 1 2
//3 4 5
//6 7 8