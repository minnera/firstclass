﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace wpf2_1_nettv
{
    class EnumToBoolConverter : IValueConverter
    {
        /// <summary>
        /// VM => UI
        /// </summary>
        /// <param name="value">VM: NetType</param>
        /// <param name="targetType">bool</param>
        /// <param name="parameter">XAML ConverterParameter: ENUM érték hozzárendelve a UI elemhez</param>
        /// <param name="culture">Thread culture</param>
        /// <returns>bool: be legyen-e jelölve a RadioButton</returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value.Equals(parameter); //itt a == referencia egyenlőséget nézne! ezért kell equal helyette
        }

        /// <summary>
        /// UI => VM
        /// </summary>
        /// <param name="value">UI: bool, be van-e jelölve a RadioButton</param>
        /// <param name="targetType">NetType</param>
        /// <param name="parameter">XAML ConverterParameter: ENUM érték hozzárendelve a UI elemhez</param>
        /// <param name="culture">Thread culture</param>
        /// <returns>NetType, amint be kell írni a VM.Net-be</returns>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value.Equals(true))
            {
                return parameter;
            }
            else
            {
                return Binding.DoNothing;
            }
        }
    }
}
