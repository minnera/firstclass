﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Documents;

namespace wpf3_2_listview
{
    class SortListView : ListView
    {
        private SortAdorner adorner;
        private GridViewColumnHeader header;

        public void SortMe(GridViewColumnHeader h)
        {
            if (h == null || h.Tag == null || h.Tag.ToString() == String.Empty)
            {
                return;
            }
            string sortBy = h.Tag.ToString();

            if (header != null)
            {
                Items.SortDescriptions.Clear();
                AdornerLayer.GetAdornerLayer(header).Remove(adorner);
            }

            ListSortDirection dir = ListSortDirection.Ascending;
            if (header == h && adorner.Direction == ListSortDirection.Ascending)
            {
                dir = ListSortDirection.Descending;
            }
            Items.SortDescriptions.Add(new SortDescription(sortBy, dir));

            header = h;
            adorner = new SortAdorner(header, dir);
            AdornerLayer.GetAdornerLayer(header).Add(adorner);
        }
    }
}
