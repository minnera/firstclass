﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight;

namespace WpfApplication2_mvvm.Models
{
    //Bindable helyett van az Observable Object
    public class DeptVM : ObservableObject
    {
        private int deptno;
        private string dname;
        private string loc;
        private bool enabled;

        public int Deptno
        {
            get { return deptno; }
            set { Set(ref deptno, value); }
            //setvalue() helyett van a set, soha ne adjuk át a tulajdonság nevet névként

        }

        public string Dname
        {
            get { return dname; }
            set { Set(ref dname, value); }
        }

        public string Loc
        {
            get { return loc; }
            set { Set(ref loc, value); }
        }

        public bool Enabled
        {
            get { return enabled; }
            set { Set( ref enabled, value); }
        }

        
        public DeptVM Clone()
        {
            //ez létrehoz egy új példány és átmásolja , a referenciákat is csak átmásolja ha beágyazott osztályok vannak
            return MemberwiseClone() as DeptVM;
        }

        //az aktuális példány legyen olyan mint a másik
        //ne az adattagokat adjuk át hanem a tulajdonságokat!!
        public void CopyDataFrom(DeptVM other)
        {
            this.Dname = other.Dname;
            this.Loc = other.Loc;
            this.Enabled = other.Enabled;
        }
    }
}
//legyen publikus
//ez a viewmodel adatokat tárol
//a metódusok amik kellenének bele az egy viewlogic-ba kerülnek (pl. commandhoz)
//és lesz egy window is mint 3. hely.
//a VL fog kapcsolatba lépni a service réteggel és a bl-lel