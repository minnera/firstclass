﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfApplication2_mvvm.Models;

namespace WpfApplication2_mvvm.Services
{
    public interface IDeptEditorService
    {
        bool EditDept(DeptVM d);
    }
}
