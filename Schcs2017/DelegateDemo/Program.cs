﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DelegateDemo
{
    delegate void VoidStringDelegate(string s);
    delegate int IntStringDelegate(string s);
    class Program
    {
        static void Main(string[] args)
        {
            string[] ss = new string[] { "Alma", "Barack", "Cukkini" };

            AnonymousMethods(ss);
            //StringMethods sm = new StringMethods();
            //Action<string> a1 = sm.PrintFirst;
            //Action<string, int> a2;

            //Func<string> f1;//egy darab stringet fog visszaadni pl string M(){}
            //Func<int, int, string> f2; //16 paraméterig tud elmenni, string M(int a, int b){}
            //Func<string, int> f3;
            ////delegate helyett actiont is meg lehetne adni, de nem lesz jó, de mégis hagyja, erre figyelni kell
            ////mindig az even handler delegate-ot fogod használni ha valami action dolgot akarsz


            //VoidStringDelegate d1 = sm.PrintFirst;
            //d1 += sm.PrintFirst;
            //d1 += sm.PrintObject;

            //DoSomething(ss, d1);

            Console.ReadLine();
        }

        private static void AnonymousMethods(string[] ss)
        {
            string prefix = "visszafelé: ";//erre lehet hivatkozni a lentebbi névtelen methodusokban! módosíthatják.

            DoSomething(ss, delegate(string s)
            {
                Console.Write(prefix);
                Console.WriteLine(new string(s.Reverse().ToArray()));
            });

            VoidStringDelegate d1 = new VoidStringDelegate( //ennek a sornak a jobb oldala el is hagyható itt.
                delegate(string s) 
                {
                    Console.WriteLine(s.Replace("a", "xxx"));
                });
            d1 += new StringMethods().PrintFirst;
            DoSomething(ss, d1);
            //leiratkoztatni viszont a névtelen methodot sokkal nehezebb


        }

        private static void DoSomething(string[] ss, VoidStringDelegate d1)
        {
            foreach (string s in ss)
            {
                d1(s);
            }
        }
    }
}
