﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace wpf3_2_listview
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void DataGrid_OnCellEditEnding(object sender, DataGridCellEditEndingEventArgs e)
        {
            string propName = e.Column.SortMemberPath;
            if (propName == "Salary")
            {
                //TODO kivételkezelés ezen
                int newval = int.Parse((e.EditingElement as TextBox).Text); //az editingelement-tel szerkeszti a felhasználó a tartalmat
                int oldval = (e.Row.DataContext as Worker).Salary;
                if (newval < oldval) e.Cancel = true; //ezzel elkattintani lehet, de módosítani nem fogja
            }
        }

        private void DataGrid_OnAutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            if (e.PropertyName == "Bdate")
            {
                e.Column.IsReadOnly = true;
            }
        }

        private void SortListView_Click(object sender, RoutedEventArgs e)
        {
            GridViewColumnHeader h = (e.OriginalSource as GridViewColumnHeader);
            (sender as SortListView).SortMe(h);
            //CollectionViewSource : items, sort, groups
            e.Handled = true;
        }
    }
}