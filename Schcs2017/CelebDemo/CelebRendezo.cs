﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CelebDemo
{
    delegate int Valami<T>(T t, T b);
    static class CelebRendezo
    {
        //rendezni lehet tömböt úgy hogy készít másolatot, azt rendezi és azt adja vissza
        //array osztály sort-ja kell
        //IComparable -t megvalósító kell neki a comparernek
        static public T[] Rendezo<T>(T[] list, Comparison<T> comparer)
            where T : Celeb
        {
            T[] seged = new T[list.Length];
            Array.Copy(list, seged, list.Length);//ezzel átmásoltuk az elemeket.
            //rendezés
            Array.Sort<T>(seged, comparer);

            return seged;
        }

        static public T[] Rendezo<T, U>(T[] list, U comparer)
            where T : Celeb
            where U : IComparer<T>
        {
            T[] seged = new T[list.Length];
            Array.Copy(list, seged, list.Length);//ezzel átmásoltuk az elemeket.
            //rendezés
            Array.Sort(seged, comparer);

            return seged;
        }
    }
}
