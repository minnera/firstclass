﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Remoting.Channels;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace wpf3_1_sortable
{
    class UpDownControl : FrameworkElement //FrameworkElement-nek kell hogy legyen nulla paraméteres construktora
    {
        //lefele mutat e ez a updown control?
        //public bool IsDown { get; set; }  // erre így nem lehet adatkötést rakni! ezért így nem jó

        //dependency property: először mindig kell egy staticus property
        public static readonly DependencyProperty IsDownProperty = DependencyProperty.Register("IsDown", typeof(bool), typeof(UpDownControl));
            //alapból csak 3 paraméteres, de lehet több is jóval
            //ez a 3 paraméter: neve, típus, szülőtípusa/containertype
            //ez a központi tárolóban létrehoz egy tároló típust (staticus memoria térben)
            //mintha egy dictionary szerű cucc lenne
            //minden egyes updowncontrolhoz tud tárolni egy bool értéket
            
        
        //hogy szép legyencsinálunk hozzá egy sima propertyt is
        public bool IsDown
        {
            get { return (bool)GetValue(IsDownProperty); }
            set
            {
                SetValue(IsDownProperty, value);
                
            }
        }

        public event EventHandler IsDownChanged; //ezt nem a setterbe rakjuk, mert a Binding Path módszerrel történő bizgálása során nem hívódik emg a setter
        //közvetlen a dependency propertyhez kell kötni, annak a 4. paraméterébe lehet rakni
        //viszont oda példányszinten nem lehet megvalósíani, csak static szinten, mert az static, ezért ctor ba rakjuk

        //ezután ezt ilyen formákban lehet íni:
        //UpDownControl someCtrl = new UpDownControl;
        //someCtrl.UsDown = true;
        //<UpDownControl IsDown="true" IsDownChanged="XXX"/>
        //<UpDownControl IsDown="{Binding Path=xxx}"/>


        public UpDownControl()
        {
            DependencyPropertyDescriptor.FromProperty(IsDownProperty, typeof 
                (UpDownControl)).AddValueChanged(this,
                (IChannelSender, args) =>
                {
                    InvalidateVisual();
                    IsDownChanged?.Invoke(this, EventArgs.Empty); //ez szimpla esemény
                }); //AddValuechanged:mire rakom, és hogyan rakom

            //a loaded a layeout után fut le, arra lehet rákapaszkodni
            //arra is rá kell kapaszkodni amikor a felhasználó rákattint erre az eseményre
            Loaded += UpDownControl_Loaded; //+= után tabot nyomunk!
            MouseDown += UpDownControl_MouseDown;
        }

        private void UpDownControl_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            IsDown = !IsDown;
        }

        private void UpDownControl_Loaded(object sender, RoutedEventArgs e)
        {
            InvalidateVisual();
        }
        //a ctor akkor fut le
        //először lefut a ctor, utána rakja rá a windowsra, a ctorba berakott megjelenítések csakis később jelennek meg, mert ott közben nem tud megjeleníteni


        //maga a rajzolás az onrenderrel történik
        protected override void OnRender(DrawingContext drawingcontext)
        {
            //el kell döntenem mekkora lett ez a vezérlő
            double w = ActualWidth, h = ActualHeight;
            //mindig meg kell nézni ezzel, hogy rajta vagyok e már az ablakon
            if (w != 0)
            {
                Pen p = new Pen(Brushes.Crimson, 2); //ezt sokkal jobb lenne központilag példányosítani
                drawingcontext.DrawGeometry(Brushes.Gold, p, new RectangleGeometry(new Rect(0,0,w,h))); //geometry, draw, brush összerakva
                //a Rect az egy structura, és arra használjuk, hogy téglalapot tároljon
                //Rect vs RectangleGeometry vs Rectangle
                //a R.Geometry egy univerzális dolognak a téglalap változata
                //a Rectangle egy vezérlő típus

                GeometryGroup g = new GeometryGroup();

                g.Children.Add(new LineGeometry(new Point(0, h/2), new Point(w, h/2)));

                Point c = IsDown ? new Point(w/2, h) : new Point(w/2, 0);
                //windowson az oriog mindig a bal felső sarok

                g.Children.Add(new LineGeometry(new Point(0, h/2), c));
                g.Children.Add(new LineGeometry(new Point(w, h / 2), c));

                drawingcontext.DrawGeometry(Brushes.ForestGreen, p, g);
                //+ <local:UpDownControl IsDown="False"
            }
        }
    }
}