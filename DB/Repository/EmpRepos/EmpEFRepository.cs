﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data;
using Repository.GenericRepos;

namespace Repository.EmpRepos
{
    public class EmpEFRepository : EFReporitory<EMP>, IEmpRepository
    {
        public EmpEFRepository(DbContext newctx) : base(newctx)
        {
        }

        public override EMP GetById(int id)
        {
            //itt azt érjük el hogy itt ne dobjon hibát, majd ami ezt meghivja ott már lehet hiba kidobás.
            
            return Get(akt => akt.EMPNO == id).SingleOrDefault();
        }
    }
}
