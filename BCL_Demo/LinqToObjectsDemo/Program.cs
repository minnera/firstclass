﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LinqToObjectsDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            List<int> ilist = new List<int>() { 0,1,2,3,4,5,6,7,8,9};
            List<int> ilist2 = Enumerable.Range(7, 5).ToList();

            //where, itt kell emgadni a feltételeket
            //ez a System.Linq-ben van
            IEnumerable<int> q1 = ilist.Where(i => i%2 == 0);
            q1.Dump();

            ilist.Add(12);

            q1.Dump();//itt újra meghívódik a lekérdezés, a where-es. kb a q1 üres alapjáraton, amikor akarunk tőle valamit, akkor szűr le bele mindig.

            //ez ugyanaz, mint a fentebbi. Mindig select-tel kell befejezni.
            IEnumerable<int> q2 = from i in ilist where i % 2 == 0 select i;
            //lehetne az is a vége, hogy select i * i, ekkor az elemek négyzetét adja vissza eredményül.
            q2.Dump();

            //ilyet is lehet, mikor vissza egy új dolgot adunk vissza, ekkor azt az új dolgot kell várnunk, vagyis var-t.

            var q3 = from i in ilist where i % 2 == 0 select new { Number = i, Square = i * i };
            q3.Dump();

            //vagy az első féle képp leírva ismeretlen típus
            var q4 = ilist.Where(i => i % 2 == 0).Select(i => new { Number = i, Square = i * i });
            q4.Dump();

            string[] szavak = {"gránátalam", "áfonya", "moha", "bodza", "sárga", "tizennégy", "boa", "cz", "cs" };

            //ezeket most rendezni akarjuk
            //a linq az sose nyúl hozzá az eredeti listához, mindig csinál egy újat

            Console.WriteLine(Thread.CurrentThread.CurrentCulture.Name);

            var q5 = from sz in szavak orderby sz select sz;
            q5.Dump();
            //ennél tényleg magyar szerint csinálja, mert a c-sek előbb vannak, mint a cs-s betűk

            var q6 = from sz in szavak orderby sz[0], sz[1] select sz;
            //ezt a másik féleképp: OrderBy().ThenBy() 
            q6.Dump();

            var q7 = from sz in szavak orderby sz[1] select sz;
            q7.Dump();

            //adjunk comparert neki akkor
            var q8 = szavak.OrderBy(sz => sz[1], new MagyarComparer());
            q8.Dump();

            //csökkenősorrend: OrderByDescending vagy
            //order valami descending

            //lehet egymás után is rendezni egyszerre
            //orderby valami orderby valamimás
            //vagy
            //OrderBy().OrderBy()

            //reverse-szel lehet megfordítani listát.

            //lehet csoportosítani is.
            //group by-jal, a végére nem kell selectet rakni.

            var q9 = from i in ilist group i by i % 2 == 0;
            q9.Dump();
            foreach (IGrouping<bool, int> group in q9)
            {
                Console.WriteLine(group.Key);
                foreach (int i in group)
                {
                    Console.WriteLine("\t" + i);
                }
            }


            //tovább is lehet vinni a csoportokat
        
            var q10 = from i in ilist group i by i % 2 == 0 into g orderby g.Count() where g.Count() > 4 select new { Key = g.Key, Count = g.Count() };
            q10.Dump();


            //listák összekapcsolása

            List<int> squares = new List<int> { 9, 16, 64, 144, 169};
            var q11 = from i in ilist join j in squares on i * i equals j select i;
            q11.Dump();


            var q12 = from i in ilist group i by i % 2 == 0 into g let sum = g.Sum() orderby sum select new { Key = g.Key, Count = g.Count(), Sum = sum };
            q12.Dump();


            //ezeket a lekérdezéseket össze is lehet fűzni
            //pl egy lekérdezés a bemenete egy másik lekérdezésnek

            var q13 = from n in (from i in ilist join j in squares on i * i equals j select i) where n % 2 == 0 select n;
            //e helyett lehetne var q13 = from n in q11 where n % 2 == 0 select n;
            q13.Dump();


            //ilyen is van, itt csak a legelső elemet adja vissza.
            var q14 = (from n in (from i in ilist join j in squares on i * i equals j select i) where n % 2 == 0 select n).First();


            //Particionalas();

            bool any = ilist.Any();//van-e elem a listában 
            //fel is lehet paraméterezni
            bool any2 = ilist.Any(i => i * i == 64);//van e ilyen elem, ami négyzete 64

            bool all = ilist.All(i => i < 12);//igaz-e ez minden elemre, hogy 12-nél kisebb

            //elemválasztó metódusok, First(), Last(), ElementAt(3) < ez hogy hanyadik elemet kérjük le pontosan., Single() < megnézi, hogy a lista pontosan egy elemmel rendelkezik-e, ha nem, akkor kivételt dob, ha igen, azt visszaadja,
            //ezeknek mind van egy változata, ami OrDefault-ra végződik, ekkor nem kivételt dob, hanem default értéket ad vissza. pl null-t.

            int x = (from y in ilist where y > 10 select y).First();

            var sum2 = ilist.Sum();
            var min = ilist.Min();
            var max = ilist.Max();
            var average = ilist.Average();
            //van még .ToList, ToArray, ToDict. (kell ennek kulcs is)

            //oftype, kiszedi az olyan típusú elemeket.

            List<object> ok = new List<object> { 1, 2, 3, "alma", "bodza"};
            var intek = ok.OfType<int>();

            Console.ReadLine();
        }
        //nem jó, ha megpróbáljuk az egész adatbázist megjeleníteni, mert hatalmas lehet
        //ezért részekre szedjük, és mindig csak egy részt mutatunk

        //skip while
        //feltételekkel is megadható, ekkor x elemtől adja csak vissza.
        //olyan is van ami csak egy bizonyos elemig adja vissza:
        //take while
        private static void Particionalas()
        {
            var list = Enumerable.Range(0, 100);//első index, méret
            //ezt a list-et szedjük szét
            for (int i = 0; i < list.Count(); i+=10)//Count() metódus bármi elemszámát megadja.
            {
                var group = list.Skip(i).Take(10); //az utolsó csoport lesz hiányos, ha nem osztható a megadott számmal
                foreach(int n in group)
                {
                    Console.WriteLine(n + ", ");
                }
                Console.ReadLine();
            }
        }
    }
    //ezzel már jó az ékezetesek rendezése.
    class MagyarComparer : IComparer<char>
    {
        public int Compare(char x, char y)
        {
            return new CultureInfo("hu-HU").CompareInfo.Compare(x.ToString(), y.ToString());
        }
    }

    static class EnumerableExtender
    {
        public static void Dump<T>(this IEnumerable<T> list)
        {
            Console.WriteLine("----------------------");
            foreach (T item in list)
            {
                Console.WriteLine(item);
            }
        }
    }
}
