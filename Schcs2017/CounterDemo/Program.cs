﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CounterDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            //ha csak fölfele számolunk akkor az egy idő után nem lesz jó, mert törlődnek majd az objektumok, és azt így nem vonjuk le
            for (int i = 0; i < 10000000; i++)
            {
                new Counter();
                Console.WriteLine(Counter.Count);
            }
            Console.ReadLine();
        }
    }
}
