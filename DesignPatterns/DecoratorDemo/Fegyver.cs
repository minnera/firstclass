﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DecoratorDemo
{
    abstract class Fegyver //ez lehetne akár interface is
    {
        abstract public void Tuzeles();
    }

    class Puska : Fegyver
    {
        public override void Tuzeles()
        {
            Console.WriteLine("Én egy puska vagyok, és kilövök egy golyót.");
        }
    }
    class Golyoszoro : Fegyver
    {
        public override void Tuzeles()
        {
            Console.WriteLine("RATTATATATATTATTTATATATATATATTAA");
        }
    }

    abstract class Fegyverkiegeszítő : Fegyver
    {
        private readonly Fegyver _fegyver;

        //abstract osztály constructora általában szebb ha protectiv
        public Fegyverkiegeszítő(Fegyver fegyver)
        {
            _fegyver = fegyver;
        }
        public override void Tuzeles()
        {
            _fegyver.Tuzeles();
        }
    }

    class Tavcso : Fegyverkiegeszítő
    {
        public Tavcso(Fegyver fegyver) : base(fegyver)
        {

        }

        public override void Tuzeles()
        {
            Console.WriteLine("Távcső vagyok, nézek messzire.");
            base.Tuzeles();
        }
    }

    class Granatveto : Fegyverkiegeszítő
    {
        public Granatveto(Fegyver fegyver) : base(fegyver)
        {

        }

        public override void Tuzeles()
        {
            base.Tuzeles();
            Console.WriteLine("...és kap egy gránátot is, hogy tudja hol a helye.");
        }
    }
}
