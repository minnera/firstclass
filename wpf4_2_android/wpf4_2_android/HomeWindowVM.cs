﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace wpf4_2_android
{
    class HomewindowVM : Bindable
    {
        //HomeWindowVM (apps, apptoexecute, apptodelete)
        private BindingList<MobileApp> apps;

        public BindingList<MobileApp> Apps
        {
            get { return apps; }
            set { SetValue(ref apps, value); }
        }

        public MobileApp AppToExecute { get; set; }

        public MobileApp AppToDelete { get; set; }
        //public void AppToExecute(MobileApp start)
        //{
        //    //
        //    MessageBox.Show("Elindult az app.");
        //}

        //public void AppToDelete(MobileApp old)
        //{
        //    apps.Remove(old);
        //    MessageBox.Show("Törlődött az app.");
        //}

        public HomewindowVM()
        {
            Apps = new BindingList<MobileApp>();
            Apps.Add(new MobileApp() {Author = "Ferenc", CreationDate = DateTime.Now, Name = "Instagram", Size = 10});
            Apps.Add(new MobileApp() { Author = "Béla", CreationDate = DateTime.Now, Name = "Facebbok", Size = 12 });
            Apps.Add(new MobileApp() { Author = "Tamás", CreationDate = DateTime.Now, Name = "Messenger", Size = 8 });
            Apps.Add(new MobileApp() { Author = "Károly", CreationDate = DateTime.Now, Name = "Chrome", Size = 22 });
            Apps.Add(new MobileApp() { Author = "Ottó", CreationDate = DateTime.Now, Name = "Games", Size = 45 });
        }
    }
}
