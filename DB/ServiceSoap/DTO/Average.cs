﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ServiceSoap.DTO
{
    [DataContract]
    public class Average
    {
        [DataMember]
        public double AvgSal { get; set; }
        [DataMember]
        public string Job { get; set; }
    }
}
//mindig az a leggyorsabb, ha te kézzel megcsinálod, hogy minden egyes elemhez létrehozol egy-egy tulajdonságot konkrétan
//gyakorlatban ezt annyira mégsem szeretjük, hanem automata dolgokkal másolgatunk.
//ez az automapper