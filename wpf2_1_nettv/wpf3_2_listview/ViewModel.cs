﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace wpf3_2_listview
{
    class ViewModel : Bindable
    {
        private Worker selectedWorker;
        public ObservableCollection<Worker> WorkerList { get; private set; }

        public Worker SelectedWorker
        {
            get { return selectedWorker; }
            set { SetValue(ref selectedWorker, value); }
        }

        public ViewModel()
        {
            WorkerList = new ObservableCollection<Worker>();

            Random R = new Random();
            WorkerList.Add(new Worker() { Name = "Victor", Bdate = DateTime.Now.AddDays(-20000*R.NextDouble()), Salary = R.Next(1000, 10000)});
            WorkerList.Add(new Worker() { Name = "Kate", Bdate = DateTime.Now.AddDays(-20000 * R.NextDouble()), Salary = R.Next(1000, 10000) });
            WorkerList.Add(new Worker() { Name = "Thomas", Bdate = DateTime.Now.AddDays(-20000 * R.NextDouble()), Salary = R.Next(1000, 10000) });
            WorkerList.Add(new Worker() { Name = "Ann", Bdate = DateTime.Now.AddDays(-20000 * R.NextDouble()), Salary = R.Next(1000, 10000) });
            WorkerList.Add(new Worker() { Name = "Leon", Bdate = DateTime.Now.AddDays(-20000 * R.NextDouble()), Salary = R.Next(1000, 10000) });
            WorkerList.Add(new Worker() { Name = "Zeo", Bdate = DateTime.Now.AddDays(-20000 * R.NextDouble()), Salary = R.Next(1000, 10000) });
            WorkerList.Add(new Worker() { Name = "Ria", Bdate = DateTime.Now.AddDays(-20000 * R.NextDouble()), Salary = R.Next(1000, 10000) });
            WorkerList.Add(new Worker() { Name = "Tamara", Bdate = DateTime.Now.AddDays(-20000 * R.NextDouble()), Salary = R.Next(1000, 10000) });

        }
    }
}
