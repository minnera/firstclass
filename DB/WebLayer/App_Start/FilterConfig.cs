﻿using System.Web;
using System.Web.Mvc;
using WebLayer.Classes;

namespace WebLayer
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            filters.Add(new VisitCounterFilterAttribute());
            //session hijacking
        }

    }
}
