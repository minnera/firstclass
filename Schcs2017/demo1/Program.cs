﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using DemoLibrary; //ekkor nem kell a DemoLibrary. az Asztal elé.
//anvilofdawn
namespace Demo1
{
    class Program
    {
        static void Main(string[] args)
        {
            //vannak érték és referencia típusok
            object o = new object(); //ha class, akkor ref.
            //ha struck akkor érték t. pl struct System.Int32
            //int i = 6;
            DemoLibrary.Asztal a = new DemoLibrary.Asztal();//ha be van rakva referencesbe a másik projekt akkor namespace.-tal lehet hivatkozni rá, vagy using elején és az se kell.

            int[] intek = new int[] {1, 2, 3};
            Console.WriteLine(intek[0]);
            //Array.Resize(ref intek, 66); //ez kicserél, csinál új tömböt

            object[] ok = new string[2];
            //nem hasnzáljuk, mert ezt engedi leírni de futásban hibát dob!
            //ok[0] = new object[];
        }
    }
}
