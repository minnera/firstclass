﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfApplication2_mvvm.Models;
using WpfApplication2_mvvm.Windows;

namespace WpfApplication2_mvvm.Services
{
    public class DeptEditorService : IDeptEditorService
    {
        public bool EditDept(DeptVM d)
        {
            DeptEditorWindow win = new DeptEditorWindow(d);
            return win.ShowDialog().Value;//amit a dialogresultba beraktam azt fogja visszaadni a showdialog
        }
    }
}
