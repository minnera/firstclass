﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;

namespace WpfApplication2_mvvm.Models
{
    class AutoMapperConfig
    {
        public static IMapper GetMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Data.DEPT, DeptDTO>().ReverseMap(); //reversemap vagyis odavissza
                cfg.CreateMap<Data.DEPT, DeptVM>().ReverseMap();
                cfg.CreateMap<DeptVM, DeptDTO>().ReverseMap();
            });
            return config.CreateMapper();
        }
    }
}
