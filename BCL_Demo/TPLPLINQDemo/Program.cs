﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TPLPLINQDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            List<int> numbers = Enumerable.Range(2000000, 10000000).ToList();
            //ConcurrentBag<int> primes = new ConcurrentBag<int>();//ez szálbiztos lista

            Stopwatch sw = Stopwatch.StartNew();

            List<int> primes = (from n in numbers.AsParallel() where n.IsPrime() select n).ToList();//+AsParallel lett
            
            sw.Stop();

            Console.WriteLine("Primes : {0}, elapsed ms: {1}", primes.Count, sw.ElapsedMilliseconds);

            Console.ReadLine();
        }
    }
    static class Int32Extender
    {
        public static bool IsPrime(this int n)
        {
            if (n <= 1)
            {
                return false;
            }
            else if (n == 2)
            {
                return true;
            }
            else if (n % 2 == 0)
            {
                return false;
            }

            for (int i = 3; i <= Math.Sqrt(n); i += 2)
            {
                if (n % i == 0)
                {
                    return false;
                }
            }
            return true;
        }
    }
}
