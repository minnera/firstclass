﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace _11_wpf5_reflect
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            //System.Windows.Interactivity.Behavior<TextBlock> x;
            //System.Windows.Interactivity.Interaction.GetBehaviors(null);
        }

        private void EditPerson_Click(object sender, RoutedEventArgs e)
        {
            Person p = new Person() {Name = "Kinga", Weight = 75, Height = 170, DoB = DateTime.Now.AddDays(-12345)};
            new EditorWindow(p).ShowDialog();
            MessageBox.Show(p.ToString());
        }

        private void EditCar_Click(object sender, RoutedEventArgs e)
        {
            var c = new {Brand = "BMW", Model = "316i", Price=34567};
            new EditorWindow(c).ShowDialog();
            MessageBox.Show(c.ToString());
        }
    }
}
