﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SingletonDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            Dungeon d1 = new Dungeon();
            d1.Greet();
            Dungeon d2 = new Dungeon();
            d2.Greet();

            Town t1 = Town.Instance;
            t1.Greet();
            Town t2 = Town.Instance;
            t2.Greet();
            //mind a kettő ugyanarra az egy városra hivatkozik.

            Console.ReadLine();
        }
    }
}
