﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace wpf2_1_nettv
{
    /// <summary>
    /// Interaction logic for ListWindow.xaml
    /// </summary>
    public partial class ListWindow : Window
    {
        public ListWindow(List<Order> orders=null)
        {
            InitializeComponent();

            (FindResource("VM") as ListWindowViewModel).Items = orders;
        }
    }
}
