﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TPLTaskDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            ////most szál helyett taskot fogunk létrehozni

            //Console.WriteLine("Main: " + Thread.CurrentThread.ManagedThreadId);

            //// a Taskok majdnem ugyanolyanok most, mint a Thread-ek
            //Task t1 = new Task(Worker.DoWork, ConsoleColor.Cyan);
            //Task t2 = new Task(Worker.DoWork, ConsoleColor.Yellow);

            //t1.Start();
            //t2.Start();

            ////bevárjuk a kettő taskunkat

            ////t1.Wait();
            ////t2.Wait();
            //ha több taskod is van, azokat egyszerre is be tudod várni
            //Task.WaitAll(t1, t2);

            //Console.WriteLine("Mindnek vége");

            //Task<int> t1 = new Task<int>(() => { return 42; });//névtelen metódus
            //t1.Start();
            //Console.WriteLine(t1.Result);

            CancellationTokenSource cts = new CancellationTokenSource();

            Task[] tasks = new Task[3];
            for (int i = 0; i < tasks.Length; i++)
            {
                tasks[i] = Task.Factory.StartNew(() =>
                { 
                    for (int j = 0; j < 10; j++)
                    {
                        if (cts.IsCancellationRequested)
                        {
                            Console.WriteLine("Megszakadok");
                            throw new OperationCanceledException(cts.Token);
                        }
                        cts.Token.ThrowIfCancellationRequested();//ezt is lehet, eléggé leíró method
                        Console.WriteLine(Thread.CurrentThread.ManagedThreadId);
                        Task.Delay(500).Wait();
                        if (j > 8)
                        {
                            throw new InvalidOperationException();
                        }
                    }
                });
            }
            //tasks[0].IsFaulted //elszállt e kivételle vagy nem, azt mutatja
            //tasks[0].IsCanceled //cancelálták e a taskot?

            Task.Delay(1500).Wait();
            //cts.Cancel();

            try
            {
                tasks[0].Wait();
            }
            catch (AggregateException aex)
            {
                Console.WriteLine(aex.InnerException.Message);

                foreach (Exception e in aex.InnerExceptions)
                {
                    Console.WriteLine(e.Message);
                }
            }

            Console.ReadLine();
        }
    }
    class Worker
    {
        private static readonly object _locker = new object();//Csak privátként csináljunk ilyet
        public static void DoWork(object o)
        {
            ConsoleColor cc = (ConsoleColor)o;
            for (int i = 0; i < 10; i++)
            {
                //ezt a részt egyszerre egy szál tudja futtatni
                //lock (typeof(Console))//itt a console-t zárjuk le most épp.
                //javítás:
                lock (_locker)
                {
                    Console.ForegroundColor = cc;
                    Console.WriteLine(Thread.CurrentThread.ManagedThreadId);
                }
                //Thread.Sleep(200);
                Task.Delay(200).Wait();
            }
        }

        public static void DoWorkLockingConsole(object o)
        {
            ConsoleColor cc = (ConsoleColor)o;
            for (int i = 0; i < 10; i++)
            {
                //ezt a részt egyszerre egy szál tudja futtatni
                //lock (typeof(Console))//itt a console-t zárjuk le most épp.
                //javítás:
                if (Monitor.TryEnter(typeof(Console), 300))
                {
                    try
                    {
                        Console.ForegroundColor = cc;
                        Console.WriteLine(Thread.CurrentThread.ManagedThreadId);

                    }
                    finally
                    {

                        Monitor.Exit(typeof(Console));
                    }
                }
                Thread.Sleep(200);
            }
        }
    }
}
