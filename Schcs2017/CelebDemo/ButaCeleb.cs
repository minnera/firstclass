﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CelebDemo
{
    class ButaCeleb : Celeb
    {
        private int _hetiBulizasokSzama;

        public int HetiBulizasokSzama
        {
            get
            {
                return _hetiBulizasokSzama;
            }

            set
            {
                _hetiBulizasokSzama = value;
            }
        }

        public ButaCeleb() : base("Győzike", 10)
        {

        }

        public ButaCeleb(string nev) : base(nev, 10)
        {

        }

        public override void SzerepelABlikkben()
        {
            Console.WriteLine("A(z) " + this.Nev + " bután szerepel a Blikkben.");
            SzerepIndex++;
            SzerepIndex--; // ez valójában nem csökkenti az értéket
        }

        public override string Tanit()
        {
            return Tanit(Iq / 2); //ez a base.Tanit(); de a base nem kell, ahogy a this sem.
        }
    }
}
