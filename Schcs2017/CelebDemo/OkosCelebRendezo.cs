﻿using System.Collections.Generic;

namespace CelebDemo
{
    internal class OkosCelebRendezo : IComparer<OkosCeleb>
    {
        public int Compare(OkosCeleb x, OkosCeleb y)
        {
            return x.Nev.CompareTo(y.Nev);
        }
    }
}