﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using WpfApplication2_mvvm.Models;

namespace WpfApplication2_mvvm.ViewModel
{
    public interface IMainViewModel
    {
        DeptVM SelectedDept { get; set; }
        ObservableCollection<DeptVM> Depts { get; set; } 

        ICommand AddCommand { get; }
        ICommand RemoveCommand { get; }
        ICommand ModifyCommand { get; }
        ICommand ChangeEnabledCommand { get; }
        ICommand UpdateViaJsonCommand { get; }

    }
}
//public lesz