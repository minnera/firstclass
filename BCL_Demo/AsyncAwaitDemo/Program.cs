﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AsyncAwaitDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            StartProcess();

            Console.WriteLine("Tovább futott a main!");

            Console.ReadLine();
        }

        private async static Task StartProcess()//ha valami async-ra átírsz, írdát void helyett Task-ra is.
        {
            //ez azért rossz még így, mert bevárja a resultot, ha nincs akkor vár vár vár
            //string s1 = GetString().Result;
            //Console.WriteLine("Elindult: " + s1);
            //string s2 = ReverseString(s1).Result;
            //Console.WriteLine("Megfordult.");
            //string s3 = ToUpper(s2).Result;
            //Console.WriteLine("Eredmény:" + s3);
            //régen volt continueWith dolog, de az bonyolult volt, le lett egyszerűsítve

            string s1 = await GetString(); //await-et csak async-os methodusban lehet használni, mainben nem lehet await!
            Console.WriteLine("Elindult: " + s1);
            string s2 = await ReverseString(s1);
            Console.WriteLine("Megfordult.");
            string s3 = await ToUpper(s2);
            Console.WriteLine("Eredmény:" + s3);

        }

        private static Task<string> ToUpper(string s2)
        {
            return Task<string>.Factory.StartNew(() =>
            {
                Task.Delay(2000).Wait();
                return s2.ToUpper();
            });
        }

        private static Task<string> ReverseString(string s1)
        {
            return Task<string>.Factory.StartNew(() =>
                {
                    Task.Delay(2000).Wait();
                    return new string(s1.Reverse().ToArray());
                }
            );
        }

        private static Task<string> GetString()//GetStringAsync
        {
            return Task<string>.Factory.StartNew(() =>
                {
                    Task.Delay(2000).Wait();
                    return "Gránátalma";
                }
            );
        }
    }
}
