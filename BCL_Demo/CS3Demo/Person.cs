﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CS3Demo
{
    class Person
    {
        public int Age { get; internal set; }
        public string City { get; internal set; }
        public string Name { get; internal set; }
    }

    static class Int32Extender
    {
        //bővítő függvények, this-szel
        //static osztály kell static methoddal
        public static bool IsValidAge(this int i)
        {
            return (i < 130 && i > 0);
        }
    }
}
