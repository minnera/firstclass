﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Repository.GenericRepos
{
    abstract public class ListRepository<TEntity> : IRepository<TEntity> where TEntity:class
    {
        protected List<TEntity> list;

        public ListRepository(params TEntity[] entries)
        {
            list = new List<TEntity>();
            list.AddRange(entries);
        }

        //ez kb fölösleges
        public void Dispose()
        {
            list.Clear();
            list = null;
        }

        public void Insert(TEntity newentity)
        {
            list.Add(newentity);
        }

        public void Delete(int id)
        {
            TEntity oldentity = GetById(id);
            if (oldentity == null)
            {
                throw new ArgumentException("NO DATA");
            }
            Delete(oldentity);
        }

        public void Delete(TEntity oldentity)
        {
            list.Remove(oldentity);
        }

        //ezt sem tudjuk itt megcsinálni szépen, könnyen.
        abstract public TEntity GetById(int id);

        public IQueryable<TEntity> GetAll()
        {
            return list.AsQueryable();
        }

        public IQueryable<TEntity> Get(Expression<Func<TEntity, bool>> condition)
        {
            return list.Where(condition.Compile()).AsQueryable();//mivel funct-ot vár nem expressiont/kifejezésfát
        }
    }
}
//DDD = Domain Driven Design
//DRY = Dont Repeat Yourself
//egymásnak már ellenmennek