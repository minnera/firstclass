﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace WpfApplication1
{
    class ViewModel : INotifyPropertyChanged
    {
        private double _income;
        private double _taxPct;
        private double _prepaid;
        private string _result;
        private bool _mustPay;

        public double Income
        {
            get { return _income; }
            set { _income = value; OnPropertyChanged(); }
        }

        public double TaxPct
        {
            get { return _taxPct; }
            set { _taxPct = value; OnPropertyChanged(); }
        }

        public double Prepaid
        {
            get { return _prepaid; }
            set { _prepaid = value; OnPropertyChanged(); }
        }

        public string Result
        {
            get { return _result; }
            set { _result = value; OnPropertyChanged(); }
        }

        public bool MustPay
        {
            get { return _mustPay; }
            set { _mustPay = value; OnPropertyChanged(); }
        }


        public event PropertyChangedEventHandler PropertyChanged;
        
        void OnPropertyChanged([CallerMemberName] string name = "")
        {
            var handler = PropertyChanged;
            if(handler != null)
                handler(this, new PropertyChangedEventArgs(name));
        }
    }
}
