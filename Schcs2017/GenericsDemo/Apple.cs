﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenericsDemo
{
    class Apple : IComparable<Apple>
    {
        private int _size;

        public int Size
        {
            get { return _size; }
            set { _size = value; }
        }

        private int _diameter;

        public int Diameter
        {
            get { return _diameter; }
            set { _diameter = value; }
        }

        public Apple()
        {

        }

        public Apple(int diam, int size)
        {
            _size = size;
            _diameter = diam;
        }

        public int CompareTo(Apple other)
        {
            if (other.Size < this.Size)
            {
                return 1;
            }
            else if (other.Size > this.Size)
            {
                return -1;
            }
            return 0;
        }
    }
}
