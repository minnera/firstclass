﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WebLayer.DTO
{
    public class Average
    {
        public double AvgSal { get; set; }
        
        public string Job { get; set; }
    }
}