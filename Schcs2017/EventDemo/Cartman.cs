﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventDemo
{
    //ez helyett action-t is haználhatnánk
    delegate void CartmanDelegate(Human cm, PulseChangedEventArgs pcea);
    class Human
    {
        private string _name;

        private int _pulse = 80;

        public string Name
        {
            get
            {
                return _name;
            }

            set
            {
                _name = value;
            }
        }

        public int Pulse
        {
            get { return _pulse; }
            set
            {
                int previous = _pulse;
                int delta = value - previous;
                _pulse = value;
                if(PulseChanged != null)
                    PulseChanged(this, new PulseChangedEventArgs(previous, delta));//eseményt
                //ha erre senki sincs feliratkozva, akkor hibát dob a settelés! Ezért kell ezt csekkolni.
                //itt azt látjuk mi lett a pulzusa, de azt nem tudjuk megkapni korábban mi volt.
                //ezért a korábbit is átadjuk paraméterrel.
            }
        }
        //ez lehetne Action is, a CartmanDelegate
        public event EventHandler<PulseChangedEventArgs> PulseChanged;

    }
}
