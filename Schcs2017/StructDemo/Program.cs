﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StructDemo
{
    struct Point
    {
        public int X;
        
        public int Y;

        

        public Point(int x, int y)
        {
            X = x;
            Y = y;
        }
    }

    struct MyStruct
    {
        public Point p;
    }

    class MyClass
    {
        public Point p;
    }
    class Program
    {
        static void Main(string[] args)
        {
            Point p1 = new Point();
            p1.X = 8;
            p1.Y = 3;

            Point p2 = new Point();
            p2.X = 5;
            p2.Y = 1;

            p1 = p2;

            p2.X = 11;
            Console.WriteLine(p1.X);
            //ez 11-et ír ki ha a Point class, mert a p1 már nem egy önálló object, hanem egy rámutató a p2-re
            //ha struct, akkor 5-öt ír ki.

            MyStruct ms = new MyStruct(); //ekkor rögtön a stacken lefoglalódik egy point-nyi hely. (2 intnyi)

            MyClass mc = new MyClass();//ekkor csak egy referencia fog létrejönni a stacken, a két intnyi helyet a memorián

            int a = 42;
            object o = new object();
            o = a;
            //a stackről nem lehet stackre mutatni, ekkor egy másolat a-ról készül hípre.
            int b = (int)o;
            b++;

            Console.ReadLine();
        }
    }
}
