﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebLayer.Models
{
    public class CrudModel
    {
        public List<DeptModel> List { get; set; }
        public DeptModel EditObject { get; set; }
    }
}