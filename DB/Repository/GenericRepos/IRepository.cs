﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Repository.GenericRepos
{
    //CRUD = create, read, update, delete
    //nem tudjuk előre hogy milyen típusokkal fogunk dolgozni
    public interface IRepository<TEntity> : IDisposable where TEntity:class
    {
        void Insert(TEntity newentity);
        void Delete(int id);
        void Delete(TEntity oldentity);
        TEntity GetById(int id);
        IQueryable<TEntity> GetAll();
        IQueryable<TEntity> Get(Expression<Func<TEntity, bool>> condition);//kifejezés fát várunk itt így.
        /*
            List<int> szamok = new List<int>();

            vagy

            IQueriable<int> sz = .....;

            ...
            var op = szamok.Where(x => x%2 == 1)
            var output = szamok.Where(x => x%2 == 1).Where(x => x > 10)
            foreach(var v in output){} //csak itt fog az előző végrehajtódni
        
        */

    }
}
