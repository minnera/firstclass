﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace AlcoholDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            //Tombok();
            //Gyujtemenyek();
            //Ciklus();
            //Szviccs();
            //Enums();
            //Methods();
            //MethodOverriding();
            //Interfaces();
            StructInterface();

            Console.ReadLine();
        }

        private static void StructInterface()
        {
            RefValue v = new RefValue(5);
            v.Increment();
            Console.WriteLine(v.X);
            Increment(v);
        }

        private static void Increment(IIncrementable v)//csak akkor működik, ha nem structot adunk át.
        {
            v.Increment(); //ez csak a másolatot módosítja, mivel a structúra nem ref. típusú, ha Value-t adnánk át
        }

        private static void Interfaces()
        {
            Alcohol a = new Beer();
            Tree t = new Tree();
            a.Burn();
            t.Burn();
            t.Temperature = int.MaxValue; //az int max felvehető értéke
            Console.WriteLine("A fa {0} fokon ég.", t.Temperature);
            BurnEverything(new IBurnable[] {a, t});//vegyesen lehet minden ami tudja ezt az interfacet.
            IBurnable ib = a;
            ib = t;
            ib = new FruitBeer();
        }

        private static void BurnEverything(IBurnable[] t)
        {
            foreach (IBurnable b in t)
            {
                b.Burn();
            }
        }

        private static void MethodOverriding()
        {
            //Beer al = new Beer(4.5);
            Beer b = new Beer();
            //al.Drink();
            b.Drink();
        }

        private static void Enums()
        {
            Beer b = new Beer();
            b.Colour = BeerColour.Kék;
            Console.WriteLine(b.ToString());
            Type t = b.GetType();
            foreach (MethodInfo m in t.GetMethods())
            {
                Console.WriteLine(m);
            }
        }
        private static void Methods()
        {
            //Alcohol a1 = new Alcohol();
            //a1.Percent = 60;
            //Alcohol a2 = new Alcohol();
            //a2.Percent = 40;
            //Alcohol a3 = new Alcohol();
            //a3.Percent = 30;
            //Alcohol a4 = new Alcohol();
            //a4.Percent = 30;
            //Alcohol.GetStronger(a1, a2);
            //a2.SetPercent(100);//nem fog csinálni semmit
            
            //Alcohol.GetStronger(a1, a2);
            //Alcohol.GetStronger(a1, a2, a3);
            ////Alcohol.GetStronger(new Alcohol[] { a1, a2, a3, a4});
            //Alcohol.GetStronger(a1, a2, a3, a4, a1, a2);
        }

        private static void Ciklus()
        {
            //így is lehet
            int j = 8;

            for (; ;)//síró smiley :c
            {
                Console.WriteLine(j++);
                if (!(j < 10 && j % 2 == 0)) //vagy (j < 10 && j % 2 == 0) == false
                {
                    break;
                }
            }

            object[] ok = new object[] { 1, 2, 3 };

            int sum = 0;

            foreach (int c in ok)
            {
                sum += c;
            }
            //vagy

            //sum += (int)ok[0];

        }

        private static void Szviccs()
        {
            Console.WriteLine("Mennyi?");
            int i = int.Parse(Console.ReadLine());
            switch (i)
            {
                case 0://ilyenkor rögtön az 1-re ugrik, ha ugyanazt akarjuk csinálni
                case 1:
                    Console.WriteLine("Kevés");
                    break;
                case 2:
                    Console.WriteLine("Már sok");
                    break;
                default:
                    Console.WriteLine("fakutya");
                    break;
            }
        }

        private static void Gyujtemenyek()
        {
            List<int> intek = new List<int>();
            intek.Add(11);
            intek.Add(2);//bármennyiszer bármennyit
            intek.Add(-99);//ez mi volt ah
            intek[0] = 33;
            Console.WriteLine(intek[0]);

            //----Dic: nagyon gyorsak, van egy kulcs és érték

            Dictionary<string, double> dict = new Dictionary<string, double>();
            dict.Add("nulla", 0.0);
            dict.Add("egy", 1.0);
            dict.Add("ketto", 2.0);
            //már létező kulcsot nem lehet újra hozzá Add-ni
            //foreach kulcs-érték párokat ad vissza
            dict["három"] = 3.0;
            dict["három"] = 3.3;
            Console.WriteLine(dict["három"]);

            //----

            Stack<int> stakkk = new Stack<int>();
            stakkk.Push(10);
            stakkk.Push(3);
            stakkk.Push(1);
            Console.WriteLine(stakkk.Pop());//az utoljára belerakott elemet kiveszi és kiírja, a stakkk-ből is eltűnik.
            Console.WriteLine(stakkk.Pop());
            Console.WriteLine(stakkk.Pop());
        }

        private static void Tombok()
        {
            //Alcohol[] alkoholok = new Alcohol[2];
            //if(alkoholok[0] == null)
            //    alkoholok[0] = new Alcohol();

            //Console.WriteLine("Add meg a százalékot:");
            
            //alkoholok[0].Percent = double.Parse(Console.ReadLine()); ;

            //Console.WriteLine(alkoholok[0].Percent);
            
        }
    }
}
