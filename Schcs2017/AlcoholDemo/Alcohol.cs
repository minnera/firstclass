﻿using System;

namespace AlcoholDemo
{
    abstract class Alcohol : IBurnable
    {
        public readonly static int MaxPercent;//fordítás idejű konstans a const, futás idejű a readonly
        //ezeket mezőknek fogjuk hívni, msot publicok lesznek
        protected double _percent = 7.7;//lehet rögtön inicializálni, arról indul minden Alcohol, like = 7.7;

        public virtual double Percent
        {
            get
            {
                return _percent;
            }

            set
            {
                if (value > 0 && value <= MaxPercent)
                {
                    this._percent = value;
                    return;
                }
                throw new ArgumentException("nem jó érték.");
            }
        }
        private string _x;

        public string X
        {
            get { return _x; }
            set { _x = value; }
        }
        private int _temperature;
        public virtual int Temperature
        {
            get
            {
                return 100;
            }
        }
        
        static Alcohol()//ez nem igazán constructor
        {
            if (DateTime.Now.DayOfWeek == DayOfWeek.Saturday)
            {
                MaxPercent = 96;
            }
            else
            {
                MaxPercent = 20;
            }
        }

        public Alcohol(double d)
        {
            Percent = d;
        }

        public Alcohol()
        {

        }
        /// <summary>
        /// Összehasonlít két paramétert, visszaadja az erősebbet.
        /// </summary>
        /// <param name="a">Az első paraméter.</param>
        /// <param name="b">A második paraméter.</param>
        /// <returns></returns>
        public static Alcohol GetStronger(Alcohol a, Alcohol b)
        {
            if(a.Percent > b.Percent)
            {
                return a;
            }
            else
            {
                return b;
            }
        }
        public static Alcohol GetStronger(Alcohol a, Alcohol b, Alcohol c)
        {
            //TODO
            return null;
        }

        public static Alcohol GetStronger(params Alcohol[] alkoholok)
        {
            if (alkoholok == null) throw new ArgumentNullException(nameof(alkoholok));

            //mi van ha csak 1 elem van? nem lesz jó az if.
            if (alkoholok.Length == 1) return alkoholok[0];

            if(alkoholok.Length == 2)
            {
                return alkoholok[0].Percent > alkoholok[1].Percent ? alkoholok[0] : alkoholok[1];
            }

            Alcohol max = alkoholok[0];//de mi van ha null-t kaptunk, első elem sincs? akkor kell a felebbi sor
            
            for(int i = 1; i < alkoholok.Length; i++)
            {
                if(max.Percent < alkoholok[i].Percent)
                {
                    max = alkoholok[i];
                }
            }

            /*foreach (Alcohol alcohol in alkoholok)
            {
                if (max.Percent < alcohol.Percent)
                {
                    max = alcohol;
                }
            }*/// ekkor az első elemet kétszer nézzük
            return max;
        }
        //public static Alcohol GetStronger(Alcohol a, int b)
        //{
        //    //TODO
        //    return null;
        //}

        public void SetPercent(double d)
        {
            if(d > 0 && d <= MaxPercent)
            {
                Percent = d;
                //return; //ide leehtne írni, de nem fontos
            }
            else//ez sem fontos most
            {
                throw new ArgumentException("ez nem jó");//lehet else nélkül is, de akkor KELL a return az if-be
            }
        }

        public abstract void Drink();
        public void Burn()
        {
            Console.WriteLine("Ég az alkohol.");
        }
    }
}
