﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Media;

namespace wpf3_2_listview
{
    class SortAdorner : Adorner
    {
        // Path Markup Syntax
        private static Geometry ascGeometry = Geometry.Parse("M 0 4 L 3.5 0 L 7 4 Z"); //M-start, Z-befejezés, L-egyenes huzása
        private static Geometry descGeometry = Geometry.Parse("M 0 0 L 3.5 4 L 7 0 Z");
        public ListSortDirection Direction { get; private set; }

        public SortAdorner(UIElement elem, ListSortDirection dir) : base(elem)
        {
            Direction = dir;
        }

        protected override void OnRender(DrawingContext drawingContext)
        {
            if (AdornedElement.RenderSize.Width < 20)
            {
                return;
            }

            TranslateTransform trans = new TranslateTransform(
                AdornedElement.RenderSize.Width - 20,
                AdornedElement.RenderSize.Height/2);

            drawingContext.PushTransform(trans);

            Geometry g = (Direction == ListSortDirection.Ascending) ? ascGeometry : descGeometry;

            drawingContext.DrawGeometry(Brushes.Black, null, g);
            drawingContext.Pop();
        }

        //public SortAdorner(UIElement adornedElement) : base(adornedElement)
        //{
            
        //}

    }
}
