﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data;
using Repository.GenericRepos;

namespace Repository.DeptRepos
{
    public interface IDeptRepository : IRepository<DEPT>
    {
        void Modify(int id, string newname, string newloc);
    }
}
