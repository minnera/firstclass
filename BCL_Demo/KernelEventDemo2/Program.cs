﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace KernelEventDemo2
{
    class Program
    {
        static void Main(string[] args)
        {
            EventWaitHandle myevent;
            try
            {
                myevent = EventWaitHandle.OpenExisting("MYKERNELEVENT");
                Console.WriteLine("Várakozunk az eseményre");
                myevent.WaitOne();
                //ha azt akarjuk, hogy amikor elkapta az eventet, akkor kisajátítsa az egészet akkor ez:
                myevent.Reset();//de ez sem biztosítja, mert nem biztos, hogy mindig elég gyorsan resetel
                Console.WriteLine("Az event beállt, elkaptuk. Futtatom a kódomat");
                Thread.Sleep(5000);
                Console.WriteLine("Kód vége");
                myevent.Set();//neked is be kell ezután állítanod az eseményt
                Console.ReadLine();
            }
            catch (WaitHandleCannotBeOpenedException)
            {
                Console.WriteLine("Nincs esemény");
                Thread.Sleep(3000);
                return;
            }
        }
    }
}
