﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CelebDemo
{
    class OkosCeleb : Celeb
    {
        private string _iskola;

        public string Iskola
        {
            get
            {
                return _iskola;
            }

            set
            {
                _iskola = value;
            }
        }

        public OkosCeleb() : base("Cleopatra", 80)
        {

        }

        public OkosCeleb(string nev) : base(nev, 80)
        {

        }

        public override void SzerepelABlikkben()
        {
            Console.WriteLine("A(z) " + this.Nev + " okosan szerepel a Blikkben.");
            SzerepIndex++;
        }

        public override string Tanit()
        {
            return Tanit(Math.Min(Iq*2, 100));
        }
    }
}
