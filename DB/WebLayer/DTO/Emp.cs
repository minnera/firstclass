﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WebLayer.DTO
{
    public class Emp
    {
        public int Empno { get; set; }
        
        public string Ename { get; set; }
       
        public string Job { get; set; }
    }
}
