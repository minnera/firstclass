﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using GalaSoft.MvvmLight.Messaging;
using WpfApplication2_mvvm.Models;

namespace WpfApplication2_mvvm.ViewModel
{
    //minden viewmodel őse itt mvvm ben a viewmodelbase, azt mindig oda kell rakni
    public class DeptEditorViewModel : ViewModelBase, IDeptEditorViewModel
    {
        //először a szerkesztett részleget kell módosítani
        private DeptVM editedDept;
        public DeptVM EditedDept
        {
            get
            {
                return editedDept;
            }
            set
            {
                Set(ref editedDept, value);
            }
        }

        public ICommand OkCommand { get; private set; }
        public ICommand CancelCommand { get; private set; }

        public DeptEditorViewModel()
        {
            //a command picit redundáns mvvmligtban, ezért most az mvvm lightban kettő féle command is van, erre figyelni kell hogy melyiket használjuk!
            //mvvmlight.command a régi é scommandwpf az uj
            OkCommand = new RelayCommand(() => Messenger.Default.Send(true));
            CancelCommand = new RelayCommand(() => Messenger.Default.Send(false));
        }
    }
}
