﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data;
using Repository.GenericRepos;

namespace Repository.DeptRepos
{
    public class DeptEFRepository : EFReporitory<DEPT>, IDeptRepository
    {
        public DeptEFRepository(DbContext newctx) : base(newctx)
        {
        }

        public override DEPT GetById(int id)
        {
            return Get(akt => akt.DEPTNO == id).SingleOrDefault();
        }

        public void Modify(int id, string newname, string newloc)
        {
            DEPT akt = GetById(id);
            if (akt == null) { throw new ArgumentException("NO DATA");}
            if (newname != null)
            {
                akt.DNAME = newname;
            }
            if (newloc != null)
            {
                akt.LOC = newloc;
            }
            context.SaveChanges(); //ha egyszerre több irányból nyúlnak az adatbázishoz, akkor az kivételt dob itt, azt le kéne kezelni
            //ConcurrencyException lekezelése TODO
        }
    }
}
