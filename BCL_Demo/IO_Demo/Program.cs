﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace IO_Demo
{
    class Program
    {
        static void Main(string[] args)
        {
            //Meghajtok();

            //Paths();

            //FileSystemWatcherTest();

            //Fajlok();

            Encodings();

            Console.ReadLine();
        }

        private static void FileSystemWatcherTest()
        {
            FileSystemWatcher fsw = new FileSystemWatcher(@"E:\tempproba", "*.txt");//a második paraméter egy filter, hogy miket figyeljen, itt .txt fájlokat fog figyelni

            fsw.Created += Fsw_Created;//a += után tabot nyomva létrehozza nekünk.
            //hogy működjön, el kell indítani
            fsw.EnableRaisingEvents = true;
        }

        private static void Fsw_Created(object sender, FileSystemEventArgs e)
        {
            Console.WriteLine("Létrejött: " + e.Name);
        }

        private static void Fajlok()
        {
            DirectoryInfo temp = new DirectoryInfo(@"E:\tempproba");//vigyázz , mert itt is a \t egy tabulátor lenne, ezért kell a kukac.
            if (temp.Exists)//ha már létezik
            {
                //Console.WriteLine("Má volt ilyen. Törölve");
                //temp.Delete(true);//ha hiányzik a true, akkor nem fogja a mappát törölni, ha nem üres

                //FileStream fs = new FileStream(@"E:\tempproba\szoveg.txt", FileMode.Open);//most openre akarjuk megnyitni
                //fs.WriteByte(66);//csak bájtonként tudunk írni a tömbbe, write-tal is.
                //fs.WriteByte(42);
                //fs.WriteByte(22);

                //fs.Flush();//ez az eddigi módosításokat lementi

                //fs.Close();//a close is flush-ol egyet lezárás előtt! a flush küzbeni mentésre jó
                ////fs.Dispose()-zal is le lehet zárni Close helyett, nincs nagy különbség.


                ////ez a using automata lezárja a stream-et, nem kell nekünk külön dispose-olni
                //using(FileStream fs = new FileStream(@"E:\tempproba\szoveg.txt", FileMode.Open))
                //{
                //    fs.WriteByte(42);
                //}

                //ha nem bájtonként akarunk olvasni, írni, akkor streamreader pl.
                StreamReader sr = new StreamReader(@"E:\tempproba\szoveg.txt");
                Console.WriteLine(sr.ReadToEnd());//ez az egész fájlt egy stringben visszaadja

                sr.Dispose();

                StreamWriter sw = new StreamWriter(@"E:\tempproba\szoveg.txt", true);//ha true, akkor hozzáfűz, és nem ír felül
                //ilyet is lehet:
                //StreamWriter sw = new StreamWriter(new FileStream(@"....", FileMode.OpenOrCreate)); vagy FileMode.Append
                //ekkor nem kell külön lezárni a benne létrehozott filestream-et, mert ő lezárja ügyesen.

                sw.WriteLine(Environment.NewLine);//ez a \n, a sortörés

                sw.WriteLine(DateTime.Now.ToLongTimeString());//ez az órát percet secet adja meg
                sw.Close();

                sr = new StreamReader(@"E:\tempproba\szoveg.txt");

                while (!sr.EndOfStream)
                {
                    Console.WriteLine(sr.ReadLine());
                }

                sr.Close();
            }
            else
            {
                temp.Create();
                Console.WriteLine("Létrehozva: {0} {1}", temp.FullName, temp.LastWriteTime);

                FileInfo szoveg = new FileInfo(@"E:\tempproba\szoveg.txt");
                if (!szoveg.Exists)
                {
                    szoveg.Create();
                    Console.WriteLine("Létrehoztuk a szöveges fájlt.");
                }
            }

            string ez = "";
            ez = Console.ReadLine();

            Console.WriteLine(ez);
        }

        private static void Meghajtok()
        {
            foreach(DriveInfo de in DriveInfo.GetDrives())
            {
                Console.WriteLine(de.Name);
                Console.WriteLine(de.DriveType);
                if (de.IsReady)
                {
                    Console.WriteLine(de.VolumeLabel);//ha üres a cd meghajtó, akkor ez hibát fog dobni. Ezért kell csekkolni ready-e
                    foreach(DirectoryInfo info in de.RootDirectory.GetDirectories())
                    {
                        Console.WriteLine("\t[" + info.Name + "]");
                    }
                    foreach(FileInfo info in de.RootDirectory.GetFiles())
                    {
                        Console.WriteLine("\t" + info.Name);
                    }
                }
                Console.WriteLine();
            }
        }

        private static void Paths()
        {
            string path1 = @"E:\tempproba\szoveg.txt";
            //FileInfo fi = new FileInfo(path1);
            if (File.Exists(path1))
            {
                string path2 = Path.ChangeExtension(path1, ".cpy");//kicseréli a szöveg végi kiterjesztést, itt a .txt-t
                File.Copy(path1, path2);
                Console.WriteLine("másolva");
            }
        }

        private static void Encodings()
        {
            string s = "árvítűrő tükörfúrógép";
            byte[] bs = Encoding.Unicode.GetBytes(s);//ez stringből byte tömböt csinál

            if (!File.Exists("encoding.txt"))
            {
                using (FileStream fs = new FileStream("encoding.txt", FileMode.Create))
                {
                    fs.Write(bs, 0, bs.Length); //mit, mettől, meddig
                } 
            }
            else
            {
                using (FileStream fs = new FileStream("encoding.txt", FileMode.Open))
                {
                    byte[] bs2 = new byte[fs.Length];
                    fs.Read(bs2, 0, bs2.Length);//a szöveg tartalmát egy byte tömbbe rakjuk
                    string s2 = Encoding.Unicode.GetString(bs2);
                    Console.WriteLine(s2);
                }
            }
        }
    }
}//commit komment
