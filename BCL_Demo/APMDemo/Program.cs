﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace APMDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            //int length = f("Vlamadsfadsfjiaosdfjoaij");
            //var iar = f.BeginInvoke("asdafasdfa", null, null);

            //while(iar.IsCompleted != true)
            //{
            //    Console.WriteLine(".");
            //    Thread.Sleep(500);
            //}
            //int length = f.EndInvoke(iar);

            //Console.WriteLine(length);


            Func<string, int> f = new Func<string, int>(GetLength);
            f.BeginInvoke("asdafasdfa", GetLengthCompleted, null);

            Console.WriteLine("Tovább futott");


            Console.ReadLine();
        }

        private static void GetLengthCompleted(IAsyncResult ar)
        {

            Func<string, int> f = ar.AsyncState as Func<string, int>;
            int length = f.EndInvoke(ar);
            Console.WriteLine(length);
        }

        static int GetLength(string s)
        {
            Thread.Sleep(4000);
            return s.Length;
        }

        //volt szó BeginRead, EndRead dolgokról, de mi ne csináljunk ilyen párokat
    }
}
