﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CelebDemo
{
    class CelebList<T> where T : Celeb
    {
        private List<T> _lista = new List<T>();

        public List<T> Lista
        {
            get
            {
                return _lista;
            }

            set
            {
                _lista = value;
            }
        }

        public void Add(T t)
        {
            _lista.Add(t);
        }

        public T Get(int index)
        {
            if(index < _lista.Capacity)
                return _lista[index];
            else
            {
                throw new ArgumentOutOfRangeException();
            }
        }

        public U Cast<U>(int index, CastTtoUDelegate<T, U> d)
        {
            U result;
            if (index >= _lista.Capacity)
                throw new ArgumentOutOfRangeException();
            else {
                result = d(this._lista[index]);
                return result;
            }
        }
    }

    delegate U CastTtoUDelegate<T, U>(T t) where T : Celeb;
}
