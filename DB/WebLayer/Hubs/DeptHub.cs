﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BusinessLogic;
using Microsoft.AspNet.SignalR;

namespace WebLayer.Hubs
{
    public class DeptHub : Hub
    {
        //public void Hello()
        //{
        //    //az összes kliensnek (dynamic típusú: nem kapunk segítésget az írásához)a hello metodusa
        //    Clients.All.hello();
        //}
        public void DelDept(int id)
        {
            //mindent itt példányosítunk az adattagon belül
            new RealLogic().DelDept(id);
            //felkérem az összes klienst, hogy frissítse a listáját.
            Clients.All.Refresh();
        }

        public void AddDept(DTO.Dept d)
        {
            Data.DEPT realDept = DTO.AutoMapperConfig.GetMapper().Map<DTO.Dept, Data.DEPT>(d);
            new RealLogic().AddDept(realDept);
            Clients.All.DeptAdded(d);
        }
    }
}
//részleg törlése, és hozzáadása