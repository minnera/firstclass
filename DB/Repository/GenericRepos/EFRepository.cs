﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;


//tetszőleges táblát ezzel majd kezelhetek
namespace Repository.GenericRepos
{
    //általános tábláktól független entity framework tároló ez
    abstract public class EFReporitory<TEntity> : IRepository<TEntity> where TEntity:class 
        //a megszorításokat mindig meg kell ismételni
    {
        //egy ősosztályt fogunk használni tároló jelzésére
        protected DbContext context;

        public EFReporitory(DbContext newctx)
        {
            context = newctx;
        }

        //felszabadítás
        public void Dispose()
        {
            context.Dispose();
        }

        public void Insert(TEntity newentity)
        {
            context.Set<TEntity>().Add(newentity);
            context.SaveChanges();
            //nem kell a státuszt módosíani kézileg, mert automatikusan megcsinálja.
        }

        public void Delete(int id)
        {
            TEntity oldentity = GetById(id);
            if (oldentity == null)
            {
                throw new ArgumentException("NO DATA");
            }
            Delete(oldentity);
        }

        public void Delete(TEntity oldentity)
        {
            //itt a set mint = halmaz-t jelent. kiolvassa a táblát a Set.
            //a TEntity itt azt adja meg hogy mi van a táblában, mi kell nekünk
            context.Set<TEntity>().Remove(oldentity);

            //ez használható arra, hogy konkrét példányra hivatkozzak, az Entry
            //azért használunk Entryt- és nem közvetlen hivatkozást, mert ezzel jelezzük, hogy egy adatbázisban van amire hivatkozunk
            context.Entry<TEntity>(oldentity).State = EntityState.Deleted;//ez nem kell, mert magától is megcsinálja.
            context.SaveChanges();//ezzel frissítjük az adatbázist!
        }

        //ez egy bonyolult dolog
        //ez táblafüggetlenül nemigazán oldható meg, nem csináljuk meg itt.
        abstract public TEntity GetById(int id);
        //az abstract methodusokat a ctor alá szokták rakni közvetlenül.

        public IQueryable<TEntity> GetAll()
        {
            return context.Set<TEntity>();
        }

        public IQueryable<TEntity> Get(Expression<Func<TEntity, bool>> condition)
        {
            return GetAll().Where(condition);
        }
    }
}
