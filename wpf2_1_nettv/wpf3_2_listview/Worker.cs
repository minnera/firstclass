﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace wpf3_2_listview
{
    class Worker : Bindable
    {
        private string name;
        private int salary;
        private DateTime bdate;

        public string Name
        {
            get { return name; }
            set { SetValue(ref name, value); }
        }

        public int Salary
        {
            get { return salary; }
            set { SetValue(ref salary, value); }
        }

        public DateTime Bdate
        {
            get { return bdate; }
            set { SetValue(ref bdate, value); }
        }
    }
}
