﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;

namespace wpf4_1_pizza
{
    class Pizza : Bindable
    {
        private PizzaExtra size;

        public PizzaExtra Size
        {
            get { return size; }
            set
            {
                SetValue(ref size, value); 
                OnPropertyChanged("TotalPrice");
            }
        }
        //IEnumerable:
        //-belső tulajdonság módosítása UI-on látszik (PropertyChanged)
        //-gyűjtemény módosulása nem látszik

        //ObservableCollection
        //-Gyűjtemény módosulása is látszik a UI-on
            
        //BindingList
        //-belső tulajdonság módusolása kódban elkapható 
        public BindingList<PizzaExtra> Toppings { get; private set; }

        public int TotalPrice //BL layer ???
        {
            get
            {
                int price = size.Price;
                foreach (var item in Toppings)
                {
                    if (item.IsWanted) price += item.Price;
                }
                return price;
            }
        }

        public Pizza()
        {
            Toppings = new BindingList<PizzaExtra>();
            Toppings.ListChanged += Toppings_ListChanged;
        }

        private void Toppings_ListChanged(object sender, ListChangedEventArgs e)
        {
            OnPropertyChanged("TotalPrice");
        }
    }
}
//az első dolog ami elkészül: a wireframe: ez kb az ablak váza: ezt nekünk két hét múlva hét elejére kész kell lennie
