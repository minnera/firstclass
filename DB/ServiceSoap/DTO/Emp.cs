﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ServiceSoap.DTO
{
    //itt a szolgáltatást hosztoló rendszernek kell jelezni. adatátviteli szerepet fog ez betölteni a szolgáltatásban
    [DataContract]//ezek attributomok, egy címke az alatta levő {} tagra. ezek mindig valaki másnak (pl. programozo) szólnak
    public class Emp
    {
        [DataMember]//arra rakjuk ezt, amit szeretnénk, hogy látszódjon kifele is, a kliens oldalon is.
        public int Empno { get; set; }
        [DataMember]
        public string Ename { get; set; }
        [DataMember]
        public string Job { get; set; }
    }
}
