﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryDemo
{
    class Food
    {
        public string Type { get; set; }
    }

    class AnimalFood : Food
    {
        
    }

    class PlantFood : Food
    {
        
    }
}
