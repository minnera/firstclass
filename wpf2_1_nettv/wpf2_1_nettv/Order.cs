﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace wpf2_1_nettv
{
    //tv típus, internet típus, ügyfél kategória
    //egy választó lista lesz ahol lehetőségek közül tud választani
    public enum TvType { few, average, extra}
    public enum NetType { slow, average, fast}
    public enum RatingType { VIP, A, B, C}
    public class Order : Bindable
    {
        private string name;
        private string bank;
        private int months;
        private DateTime date;
        private TvType tv;
        private NetType net;
        private RatingType rating;

        public string Name
        {
            get { return name; }
            set { SetValue(ref name, value); }
        }

        public string Bank
        {
            get { return bank; }
            set { SetValue(ref bank, value); }
        }

        public int Months
        {
            get { return months; }
            set { SetValue(ref months, value); }
        }

        public DateTime Date
        {
            get { return date; }
            set { SetValue(ref date, value); }
        }

        public TvType Tv
        {
            get { return tv; }
            set
            {
                SetValue(ref tv, value); 
                OnPropertyChanged("TvFew");
                OnPropertyChanged("TvAvergae");
                OnPropertyChanged("TvExtra");
                OnPropertyChanged("Total"); //+ Net.Set, Rating.Set -be is be kell rakni ezt
            }
        }
        public bool TvFew
        {
            get { return tv == TvType.few; }
            set { if(value) Tv = TvType.few; }
        }
        public bool TvAverage
        {
            get { return tv == TvType.average; }
            set { if (value) Tv = TvType.average; }
        }
        public bool TvExtra
        {
            get { return tv == TvType.extra; }
            set { if (value) Tv = TvType.extra; }
        }

        public NetType Net
        {
            get { return net; }
            set
            {
                SetValue(ref net, value);
                OnPropertyChanged("Total");
            }
        }

        public RatingType Rating
        {
            get { return rating; }
            set
            {
                SetValue(ref rating, value);
                OnPropertyChanged("Total");
            }
        }

        public Order Clone()
        {
            //shallow vs deep copy
            return MemberwiseClone() as Order;
        }

        public override string ToString()
        {
            StringBuilder SB = new StringBuilder();
            SB.AppendFormat("NAME = {0}\n", name);
            SB.AppendFormat("BANK = {0}\n", bank);
            SB.AppendFormat("DATE = {0}\n", date.ToShortDateString());
            SB.AppendFormat("MONTHS = {0}\n", months);
            SB.AppendFormat("TV = {0}\n", tv);
            SB.AppendFormat("NET = {0}\n", net);
            SB.AppendFormat("RATING = {0}\n", rating);
            SB.AppendFormat("TOTAL = {0}\n", Total);
            return SB.ToString();
        }

        public double Total
        {
            get
            {
                double result = 1500 * (1 + (int) net) + 500 * (1 + (int) tv);
                result *= 1 + 0.02 * (1 + (int) rating);
                return result;
            }
            set { }
        }
    }
}
