﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Collections.Concurrent;//szinkronizált listák, szálbiztosak

namespace TPLParallelDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            //ParallelIteration();

            ParallelBreak();

            Console.ReadLine();
        }

        private static void ParallelBreak()
        {
            //keresés, mikor nem kell az egész dolgon végigmennünk, és ezt akarjuk párhuzamosítani
            List<int> numbers = Enumerable.Range(0, 100).ToList();

            //parallelben nem lehet breaket használni, mert ez itt már egy metódus
            Parallel.For(0, numbers.Count, (i, pls) => //parallel luck state
            {
                Console.WriteLine(numbers[i]);
                if(numbers[i] == 77)
                {
                    Console.WriteLine("Megvagy");
                    //break;
                    //pls.Break();
                    //de ekkor is csomó extrát dolgozik a párhuzamosság miatt
                    pls.Stop();
                }
            });
        }

        private static void ParallelIteration()
        {
            //prim keresés
            //a List nem szál biztos osztály, nekünk kell szálbiztossá tennünk
            List<int> numbers = Enumerable.Range(2000000, 10000000).ToList();
            //List<int> primes = new List<int>();
            ConcurrentBag<int> primes = new ConcurrentBag<int>();//ez szálbiztos lista
            Stopwatch sw = Stopwatch.StartNew();

            Parallel.ForEach(numbers, number =>
            {
                if (number.IsPrime())
                {
                    //lock (primes)//így szálbiztossá tettük a listánkat, de ennél jobb, ha maga a lista már szálbiztos
                    //{
                    //    primes.Add(number); 
                    //}
                    primes.Add(number);
                }
            });

            sw.Stop();

            Console.WriteLine("Primes : {0}, elapsed ms: {1}", primes.Count, sw.ElapsedMilliseconds);
        }
    }

    static class Int32Extender
    {
        public static bool IsPrime(this int n)
        {
            if (n <= 1)
            {
                return false;
            }
            else if(n == 2)
            {
                return true;
            }
            else if (n%2 == 0)
            {
                return false;
            }
            
            for(int i = 3; i <= Math.Sqrt(n); i+=2)
            {
                if(n%i == 0)
                {
                    return false;
                }
            }
            return true;
        }
    }
}
