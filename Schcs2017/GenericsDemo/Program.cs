﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenericsDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            //Pair();
            Nullos();

            Console.ReadLine();
        }

        private static int CastGreenAppleToInt32(Apple a)
        {
            return a.Diameter;
        }

        private static void Pair()
        {
            Papir<int> p1 = new Papir<int>();

            p1.X = 7;
            //p1.Y = "8"; //ezt nem is engedi így már.
            p1.Y = 8;

            Papir<string> p2 = new Papir<string>();

            p2.Y = "Vadkörte";
            p2.X = "Tojgli";

            //Hashtable ht = new Hashtable(); //ennél sem elég biztonságos, nem figyel a típusra, ellenben a dictionary figyel rá

            Dictionary<string, double> d = new Dictionary<string, double>();
            d.Add("nulla", 0.0);
            d.Add("kettő", 2.0);

            List<byte> bk = new List<byte>();//itt egy byte tömb lesz a háttérben
            ArrayList al = new ArrayList();//itt egy object tömb lesz a háttérben, ez több helyet foglal, 8 byte + 2 pointe (16byte) egy elem

            Apple a1 = new Apple(3, 8);
            Apple a2 = new Apple(2, 7);
            Papir<Apple> papple = new Papir<Apple>();
            papple.X = a1;
            papple.Y = a2;
            Console.WriteLine(papple.GetSmaller());
            //Console.WriteLine(papple.GetSmallerUsingAMethod(new CompareDelegate<Apple>(Apple.CompareTo))); //ez akkor jó, ha a CompareTo kettő apple-t kap és static

            int kapott = papple.Cast<int>(new CastTtoUDelegate<Apple, int>(CastGreenAppleToInt32));
            papple.Cast(CastGreenAppleToInt32); //így is meghívható, mert kikövetkezteti a megadott methodusból a típusparamétert

        }

        private static void Nullos()  
        {
            int i = 7;
            Nullable<int> j = null;
            int? k = null;
            //ekkor ezt nekünk nem szabad ++-olni pl, mert nincs benne érték, nem lesz hiba, de üres marad ++ után is. null marad.
            //ha null van benne, akkor nem lehet viszont kasztolni int-re, fitás közbeni hibát fog dobni.

        }
    }
}
