﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CelebDemo
{
    
    public abstract class Celeb : ILegzokepes
    {
        protected string _nev;
        protected int _iq = 50;
        protected int _szerepIndex = 0;

        public string Nev
        {
            get
            {
                return _nev;
            }
            
        }

        public int Iq
        {
            get
            {
                return _iq;
            }

            set
            {
                _iq = value;
            }
        }

        public int SzerepIndex
        {
            get
            {
                return _szerepIndex;
            }
            protected set
            {
                if(value > _szerepIndex)
                {
                    _szerepIndex = value;
                }
            }
        }

        public Celeb()
        {
            _nev = "Senki";
        }

        public Celeb(string name)
        {
            _nev = name;
        }
        public Celeb(string name, int iq)
        {
            Iq = iq;
            _nev = name;
        }

        abstract public string Tanit();

        public string Tanit(int i)
        {
            this.Iq = i;
            return (this.Nev + " iqja mostantól " + this.Iq + ".");
        }

        static public Celeb Legbutabb(Celeb c1, Celeb c2, params Celeb[] tomb)
        {
            //TODO , c1 és c2 nem e null!
            Celeb min = c1;
            if (c1.Iq > c2.Iq)
            {
                min = c2;
            }
            if (tomb == null)
            {
                throw new ArgumentNullException(nameof(tomb));
            }
            foreach (Celeb ccc in tomb)
            {
                if (min.Iq > ccc.Iq)
                {
                    min = ccc;
                }
            }

            return min;
        }

        static public Celeb[] Alapertelmezett(int db)
        {
            Celeb[] tomb = new Celeb[db];
            for (int i = 0; i < db; i++)
            {
                tomb[i] = new ButaCeleb();
                tomb[i]._nev = "Arnold";
                tomb[i].Iq = i;
            }
            return tomb;
        }

        public virtual void SzerepelABlikkben()
        {
            Console.WriteLine("A(z) " + Nev + " szerepel a Blikkben.");
            SzerepIndex++;
        }

        public void Lelegzik()
        {
            Console.WriteLine("Breath");
        }

        public void Tarsalog(Celeb c, VoidDelegate vd)
        {
            Console.WriteLine("Köszön");
            vd( c);
            Console.WriteLine("Elköszön");
        }
    }
}
