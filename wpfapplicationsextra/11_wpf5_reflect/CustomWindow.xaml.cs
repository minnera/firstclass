﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace _11_wpf5_reflect
{
    partial class CustomWindow : ResourceDictionary
    {
        public CustomWindow()
        {
            InitializeComponent();
        }

        private void Close_Click(object sender, RoutedEventArgs e)
        {
            Window win = (sender as FrameworkElement).TemplatedParent as Window;
            win.Close();
        }

        private void Maximize_Click(object sender, RoutedEventArgs e)
        {
            Window win = (sender as FrameworkElement).TemplatedParent as Window;
            win.WindowState = win.WindowState == WindowState.Maximized ? WindowState.Normal : WindowState.Maximized;
            
        }

        private void Minimize_Click(object sender, RoutedEventArgs e)
        {
            Window win = (sender as FrameworkElement).TemplatedParent as Window;
            win.WindowState = WindowState.Minimized;
        }
    }
}
