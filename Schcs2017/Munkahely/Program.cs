﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Munkahely
{
    class Program
    {
        static void Main(string[] args)
        {
            Melos m1 = new Bela(1, "Béla");
            Melos m2 = new Tamas(2, "Géza");
            Melos m3 = new Bela(1, "Kata");
            Melos m4 = new Tamas(3, "Márk");
            Melos m5 = new Gergely(2, "Helga");

            Munkahely google = new Munkahely();
            google.MelostKap(m1);
            google.MelostKap(m2);
            google.MelostKap(m3);
            google.MelostKap(m4);
            google.MelostKap(m5);

            Munka munka1 = new Munka(Melosok.Tesztel);
            Munka munka2 = new Munka(Melosok.Fejleszt);
            Munka munka3 = new Munka(Melosok.Tesztel);
            Munka munka4 = new Munka(Melosok.Tervez);
            Munka munka5 = new Munka(Melosok.Fejleszt);
            Console.WriteLine("Az első munka jellege: " + munka1.MunkaJellege);
            google.Dolgoztat(munka1);
            google.Dolgoztat(munka2);
            google.Dolgoztat(munka3);
            google.Dolgoztat(munka4);
            google.Dolgoztat(munka5);

            Console.ReadLine();
        }
    }
}
