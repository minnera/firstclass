﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using WpfApplication2_mvvm.Models;

namespace WpfApplication2_mvvm.ViewModel
{
    
    public interface IDeptEditorViewModel
    {
        DeptVM EditedDept
        { get; set; }

        ICommand OkCommand { get; }
        ICommand CancelCommand { get; }
    }
}
//ennek is publicnak kell lennie