﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace _11_wpf5_reflect
{
    //Behavior via Attached Property
    /// 
    /// <Grid>
    ///     <Grid.ColumnDefinitions>
    ///         xxxxxxx
    ///     </Grid.ColumnDefinitions>
    //     <ListBox Grid.Column="1" xxx/>   <<< ez itt a grid.column a b v a p
    /// </Grid>
    /// 
    //Interaction Behavior / Blend Behavior

    class DigitsOnlyBehavior
    {
        //olyan textboxot akarunk amibe csak számokat lehessen beírni
        //Dependency propoerty kell nekünk
        //minden dependency property publicstaticreadonly
        public static readonly DependencyProperty IsDigitsOnlyProperty;

        //kell get és set védő static metodus is
        public static bool GetIsDigitOnly(DependencyObject obj)
        {
            return (bool) obj.GetValue(IsDigitsOnlyProperty);
        }

        public static void SetIsDigitOnly(DependencyObject obj, bool value)
        {
            obj.SetValue(IsDigitsOnlyProperty, value);
        }

        static DigitsOnlyBehavior()
        {
            IsDigitsOnlyProperty = DependencyProperty.RegisterAttached("IsDigitsOnly", typeof(bool), typeof(DigitsOnlyBehavior), new PropertyMetadata(new PropertyChangedCallback(DigitsOnlyChanged)));
        }

        private static void DigitsOnlyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is TextBox)
            {
                // <TextBox DigitsOnlyBehavior.IsDigitsOnly="true"
                TextBox tb = d as TextBox;
                bool value = (bool) e.NewValue;

                if (value)
                {
                    tb.PreviewTextInput += Tb_PreviewTextInput;
                }
                else
                {
                    //akkor meg leiratkozunk
                    tb.PreviewTextInput -= Tb_PreviewTextInput;
                }
            }
        }

        //copypaste ez mellett is működni fog!!
        //AddPAstingHandler
        private static void Tb_PreviewTextInput(object sender, System.Windows.Input.TextCompositionEventArgs e)
        {
            //Akkor kell lekezelni, ha a textben bármelyik karakter nem szám az eseményt.
            e.Handled = e.Text.Any(c => !Char.IsDigit(c));
        }
    }
}
