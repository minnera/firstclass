﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mobileapp
{
    class HomeWindowVM : Bindable
    {
        private IEnumerable<MobilApp> apps;

        public IEnumerable<MobilApp> Apps
        {
            get { return apps; }
            set { SetValue(ref apps, value); }
        }

        public void AppToExecute()
        {
            
        }

        public void AppToDelete(MobilApp OldMA)
        {
            foreach (var v in apps)
            {
                if (v == OldMA)
                {
                    
                }
            }
        }
    }
}
