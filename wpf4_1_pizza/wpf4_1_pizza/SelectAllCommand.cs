﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace wpf4_1_pizza
{
    class SelectAllCommand : ICommand
    {
        public event EventHandler CanExecuteChanged;

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            IEnumerable<PizzaExtra> toppings = parameter as IEnumerable<PizzaExtra>;
            if (toppings != null)
            {
                foreach (var v in toppings)
                {
                    v.IsWanted = true;
                }
            }
        }
    }
}
//túl sok kicsi osztály => MVVM light -ot használjunk inkább ezért ehelyett, mert így midnen gombnak külön osztályt ,  commandot kéne létrehoznunk
// VM <=> interfész megfeleltetés nincs, az ablak a VM osztályt közvetlenül használja => IoC = Inversion of Control / Service Locator => 