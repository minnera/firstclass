﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CounterDemo
{
    class Counter
    {
        public static int Count;//megadja összesen eddig hány példány keletkezett ebből a típusból
        //lehet így is, és lehet úgy is hogy 0-át adunk neki.

        public Counter()
        {
            Counter.Count++;
        }
        static Counter()
        {
            //statikus ctor, a statikus tagok egyszeri beállítására
            Count = 1;
        }

        //nincs láthatósága, típusa, paramétere sem, nem lehet hívni
        ~Counter()
        {
            Count--;
        }

    }
}
