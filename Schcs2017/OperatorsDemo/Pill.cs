﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OperatorsDemo
{
    class Pill
    {
        private string _wineType;

        public string WineType
        {
            get { return _wineType; }
            set { _wineType = value; }
        }
        public Pill(string type)
        {
            _wineType = type;
        }

        public static implicit operator Wine(Pill p)//konverziós operátor, ez mindig egy új objektum létrehozása
        {
            return new Wine(p._wineType, new Random().NextDouble() * 12);
        }

        public static explicit operator Pill(Wine w)//konverziós operátor
        {
            return new Pill(w.Type);
        }
    }



    class Wine
    {
        private string _type;

        public string Type
        {
            get { return _type; }
            set { _type = value; }
        }

        private double _percent;

        public double Percent
        {
            get { return _percent; }
            set { _percent = value; }
        }
        public Wine(string type, double percent)
        {
            _type = type;
            _percent = percent;
        }

        public static Wine operator ++(Wine w)
        {
            w.Percent++;
            return w;
        }

        public static Wine operator +(Wine a, Wine b)
        {
            Wine w = new Wine(GetWineType(a.Type, b.Type), (a.Percent + b.Percent) / 2);
            return w;
        }

        private static string GetWineType(string type1, string type2)
        {
            if (type1 == type2)
            {
                return type1;
            }
            else
            {
                return "cuvée";
            }
        }
    }
}
