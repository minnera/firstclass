﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

namespace AutoGUI
{
    public class AutoGuiBuilder
    {
        //hozzá kell rakni mindent ami wpf, ha refeerenciát akarok építeni
        //references: WindowsBase, PresentationCore, PresentationFramework, System.Xaml
        private string GetFriendlyName(PropertyInfo prop)
        {
            //a displayname attribut-ot akarom kiolvasni ezzel a metódussal
            DisplayNameAttribute attr = prop.GetCustomAttribute<DisplayNameAttribute>();
            //vagy létezik az attributom, vagy nem.
            if (attr == null)
            {
                //ekkor a tulajdonság nevét adom vissza
                return prop.Name;
            }
            else
            {
                //ekkor a már korábban (valszeg más programozó által) beállított nevet adjuk vissza
                return attr.DisplayName;
            }
        }
        //[DisplayName("valami")] : amikor a propertyt nézzük akkor a bal oldalon szereplő név ez a név lesz az alatta levő tulajdonsághoz

        private bool IsAllowed(PropertyInfo prop)
        {
            NoAutoGuiAttribute attr = prop.GetCustomAttribute<NoAutoGuiAttribute>();
            return attr == null && prop.CanWrite;
        }

        //a fő metodus, a gui épíése
        private void BuildGUI(Panel container, object content)//panel, amire pakolok, object, amit szerkesztek
        {
            //a panel most a mi esetünkbne stackpanel lesz
            Type t = content.GetType();
            //típusból soha nincsen több példány, mindig pontosan egy van, minden változó ugyanarra a típusra mutat.
            //ha lock(t) -t írunk, akkor az egész programban ezt a típust, pl string, leblokkoltuk!
            //soha ne rakj lock-ot nem helyi változóra

            //innentől kezdve reflekciós amit csinálunk, hogy type-ot használunk

            //type t -vel nem lehet listát csinálni , pl List<t>. ezt nem lehet.

            PropertyInfo[] props = t.GetProperties();

            foreach (PropertyInfo prop in props)
            {
                //létre kéne hozni egy szerkesztő dobozt ehhez a tulajdonsághoz
                FrameworkElement editor = CreateEditor(content, prop);//szerkesztő doboz legenerálása
                if (editor != null)
                {
                    container.Children.Add(CreatePanelItem(editor, prop));//megkapja a sz.dobozt és tulajdonságot és azokat egymás mellé rakja, és visszaadja
                }
            }
        }

        private UIElement CreatePanelItem(FrameworkElement editor, PropertyInfo prop)
        {
            Label L = new Label();
            L.Content = GetFriendlyName(prop) + ":";

            DockPanel panel = new DockPanel();
            panel.Margin = new Thickness(5);
            panel.Children.Add(L);
            panel.Children.Add(editor);
            return panel;
        }

        private FrameworkElement CreateEditor(object content, PropertyInfo prop)
        {
            //Type propType = prop.GetType(); //ez hibás, mert ez így PropertyInfo lenne, nem az amit a propertyinfo tárol
            Type propType = prop.PropertyType;
            if (propType == typeof(string) || propType == typeof(int))
            {
                //akkor textboxot adok
                return CreateBox(typeof(TextBox), TextBox.TextProperty, content, prop);
            }
            if (propType == typeof(DateTime))
            {
                //DatePicker
                return CreateBox(typeof(DatePicker), DatePicker.SelectedDateProperty, content, prop);
            }
            //ezt még a végtelenségig folytathatnánk
            return null;
            //mi lenne ha switchelnénk?  propType.ToString szerint lehetne csinálni.
            //de nem használjuk, mert az nagyon alattomos. de egyszer csak elérné a 2000 sorosságot a switch és nem lehet feldarabolni, az egy blokk
            //lehetetlen refaktorálni, ezért nem szokás csinálni.
            //csak akkor használjuk, ha nem valósít meg üzleti logikát, és nem túl sok elemű, és nem is fog nőni az esetek száma


            //Strategy Design Pattern vagy Replace Conditionals With Polymorphism
        }

        private FrameworkElement CreateBox(Type type, DependencyProperty depProp, object content, PropertyInfo prop)
        {
            //abből a random type-ból kell létrehozni egy példányt.
            //De csalunk, mert tudjuk hogy frameworkElement lesz.
            FrameworkElement box = Activator.CreateInstance(type) as FrameworkElement;
            Binding binding = new Binding(prop.Name); //source property
            binding.Source = content; //source instance
            if (!IsAllowed(prop))
            {
                box.IsEnabled = false;
                binding.Mode = BindingMode.OneTime;
            }
            box.SetBinding(depProp, binding); // target control . target dep . property
            return box;
        }

        //ez lesz az egész lelke
        public static void Execute(Panel parent, object content)
        {
            StackPanel sp = new StackPanel();
            parent.Children.Add(sp);

            new AutoGuiBuilder().BuildGUI(sp, content);
        }
    }
}
//public!