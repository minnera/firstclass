﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace XmlSerializationDemo
{
    class Program
    {
        const string FILENAME = "person.xml";
        static void Main(string[] args)
        {
            //meg kell adni neki, hogy milyen típust szeretnél sorosítani, mikor létrehozod
            XmlSerializer bf = new XmlSerializer(typeof(Person));
            if (File.Exists(FILENAME))
            {
                using (FileStream fs = new FileStream(FILENAME, FileMode.Open))
                {
                    Person p = bf.Deserialize(fs) as Person;
                    Console.WriteLine("Név: " + p.Name + " Kor: " + p.Age + " Született: " + p.Birtdate);
                }
            }
            else
            {
                Person p = new Person("Cristal", DateTime.Now.Subtract(new TimeSpan(1000, 0, 0, 0)), 2);
                Console.WriteLine(p.Birtdate);
                using (FileStream fs = new FileStream(FILENAME, FileMode.Create))
                {
                    bf.Serialize(fs, p);
                }
            }

            Console.ReadLine();
        }
    }
}
