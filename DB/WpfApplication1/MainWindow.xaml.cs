﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApplication1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private ViewModel VM;

        public MainWindow()
        {
            InitializeComponent();

            //VM = new ViewModel(); //mert a Windowban létrehozzuk
            VM = (this.Content as Panel).DataContext as ViewModel;
            VM.Income = 100;
            VM.Prepaid = 10;
            VM.TaxPct = 16;

            //az ablaknál az adatkötések forrása ez a példány legyen:
            this.DataContext = VM; //mert a másik helyen be lett állíva ezért mindegy hogy be van állítva vagy nem
            //a source propok a VM-en belüli tulajdonságok lesznek
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Hello, WPF world!");
        }

        private void CalculateClick(object sender, RoutedEventArgs e)
        {
            //double income = double.Parse(txtIncome.Text);
            //double taxPct = double.Parse(txtTaxPct.Text);
            //double prepaid = double.Parse(txtPrepaid.Text);
            //double result = income * taxPct / 100 - prepaid;
            //lblResult.Content = "MUST PAY: " + result;

            double result = VM.Income * VM.TaxPct / 100 - VM.Prepaid;
            VM.MustPay = result > 0;
            VM.Result = "MUST PAY: " + result;
            MessageBox.Show(VM.Result);
        }
    }
}
