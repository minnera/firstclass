﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ThreadingDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            //Lapok();
            //CounterDemo();
            Kepernyo();

            Console.ReadLine();
        }

        private static void CounterDemo()
        {
            //azt akarjuk most, hogy ez a 10 szál egyszerre fusson.
            Thread[] ts = new Thread[10];
            for(int i = 0; i < ts.Length; i++)
            {
                ts[i] = new Thread(Counter.Increment);
                ts[i].Start();
            }
            foreach (Thread t in ts)
            {
                t.Join();
            }
            //itt egyszerre futnak, ugyanazt a staticus mezőt állítgatják, ezért kiszámíthatatlan mi lesz az eredmény
            //mindig kevesebb lesz az eredmény, mert néha visszaesik a számolás.
            Console.WriteLine(Counter.Count);
        }
        private static void Kepernyo()
        {
            Console.WriteLine("MAIN: " + Thread.CurrentThread.ManagedThreadId);
            Thread t1 = new Thread(Worker.DoWorkLockingConsole);
            Thread t2 = new Thread(Worker.DoWorkLockingConsole);

            //Monitor.Enter(typeof(Console)); //ez csontugyanaz mint a sima lock.

            lock(typeof(Console))
            { 
                t1.Start(ConsoleColor.Cyan);
                t2.Start(ConsoleColor.Yellow);

                t1.Join();
                t2.Join();

            }

            
            
            

            Console.WriteLine("All done");
        }
        private static void Lapok()
        {
            Console.WriteLine("MAIN: " + Thread.CurrentThread.ManagedThreadId);
            Thread t1 = new Thread(Worker.DoWork);//action objectnek megfelelő delegát kell neki
            Thread t2 = new Thread(Worker.DoWork);

            //ezek drágák és költségesek, lassúak kicsit, általában a main thread gyorsabb

            //a szálak nagyon random futnak egymás mellett, nem lehet tudni hogy melyik mikor mit fog csinálni a másikhoz képest
            //szinkronizálni kéne a szálak között a működést.
            //alap módszer: lock-olsz
            //de ez nem teljesen jó.

            //ha ezt az egészet is le akarjuk zárni, akkor meghalkb az egész
            //soha nem lesz elengedve a console, deadlock
            //és erre sehol semmi nem figyelmeztet.

            lock (typeof(Console))
            {
                t1.Start(ConsoleColor.Cyan);
                t2.Start(ConsoleColor.Yellow);

                t1.Join();
                t2.Join();
            }

            Console.WriteLine("All done");
        }
    }
    class Worker
    {
        private static readonly object _locker = new object();//Csak privátként csináljunk ilyet
        public static void DoWork(object o)
        {
            ConsoleColor cc = (ConsoleColor)o;
            for(int i = 0; i < 10; i++)
            {
                //ezt a részt egyszerre egy szál tudja futtatni
                //lock (typeof(Console))//itt a console-t zárjuk le most épp.
                //javítás:
                lock(_locker)
                {
                    Console.ForegroundColor = cc;
                    Console.WriteLine(Thread.CurrentThread.ManagedThreadId); 
                }
                Thread.Sleep(200);
            }
        }

        public static void DoWorkLockingConsole(object o)
        {
            ConsoleColor cc = (ConsoleColor)o;
            for (int i = 0; i < 10; i++)
            {
                //ezt a részt egyszerre egy szál tudja futtatni
                //lock (typeof(Console))//itt a console-t zárjuk le most épp.
                //javítás:
                if (Monitor.TryEnter(typeof(Console), 300))
                {
                    try
                    {
                        Console.ForegroundColor = cc;
                        Console.WriteLine(Thread.CurrentThread.ManagedThreadId);

                    }
                    finally
                    {

                        Monitor.Exit(typeof(Console));
                    }
                }
                Thread.Sleep(200);
            }
        }
    }
}
