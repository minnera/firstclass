﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoGUI;

namespace _11_wpf5_reflect
{
    class Person
    {
        [DisplayName("Név")]
        public string Name { get; set; }
        [DisplayName("Magasság")]
        public int Height { get; set; }
        [DisplayName("Súly")]
        public int Weight { get; set; }
        [DisplayName("Születési Dátum")]
        [NoAutoGui("Ez nem fog soha változni")]
        public DateTime DoB { get; set; }

        public override string ToString()
        {
            return String.Format("{0}\n{1}\n{2}\n{3}", Name, Height, Weight, DoB);
        }
    }
}
