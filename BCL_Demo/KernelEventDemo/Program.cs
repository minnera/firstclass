﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace KernelEventDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            EventWaitHandle myevent = new EventWaitHandle(false, EventResetMode.AutoReset, "MYKERNELEVENT");

            Console.WriteLine("Event létrehozva, üss entert a tovább lépéshez.");

            Console.ReadLine();

            Console.WriteLine("Event beállítva");
            myevent.Set();

            Console.ReadLine();
        }
    }
}
