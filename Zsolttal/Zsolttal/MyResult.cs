﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zsolttal
{
    /* <summary>
    * MyResults instance = xxxx.ExecSql("select * from emp");
    * foreach(Dictionary<string, object> row in instance){
    * Console.WriteLine(row["ENUME"]);
    *}
    */
    class MyResult : IEnumerable<Dictionary<string, object>>
    {
        private List<Dictionary<string, object>> rows;

        public MyResult()
        {
            rows = new List<Dictionary<string, object>>();
        }
        //felsorolható
        public IEnumerator<Dictionary<string, object>> GetEnumerator()
        {
            return rows.GetEnumerator();
        }
        //felsoroló
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
        //memória szempontjából nem jó megközelítés
        //feldolgozás szempontjából jó megközelítés
        public void AddRow(string[] fields, object[] values)
        {
            Dictionary<string, object> newrow = new Dictionary<string, object>();
            for (int i = 0; i < fields.Length; i++)
            {
                newrow.Add(fields[i], values[i]);
            }
            rows.Add(newrow);
        }
    }
}
