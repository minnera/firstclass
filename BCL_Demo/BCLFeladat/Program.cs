﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Text.RegularExpressions;
using System.Xml.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace BCLFeladat
{
    class Program
    {
        static void Main(string[] args)
        {
            //kiírjuk a kódtáblákat
            foreach (EncodingInfo info in Encoding.GetEncodings())
            {
                Console.WriteLine("Név: {0},\tkódlap: {1}", info.Name, info.CodePage);
            }
            //bekérjük az egyiket
            Console.WriteLine("Add meg a kódlap számát: ");

            int codepage = int.Parse(Console.ReadLine());//encodinginfo.codepage szám ez
            Encoding e = Encoding.GetEncoding(codepage);


            XmlSerializer xf = new XmlSerializer(typeof(Celeb[]));

            BinaryFormatter bf = new BinaryFormatter();


            //List<Celeb> lista = new List<Celeb>();
            //nézzük meg létezik e egy celeb.txt
            //FileInfo temp = new FileInfo("celebs.txt");//vigyázz , mert itt is a \t egy tabulátor lenne, ezért kell a kukac.
            if (File.Exists("celebs.xml"))
            {

                using (FileStream fs = File.Open("celebs.xml", FileMode.Open))
                {
                    Celeb[] p = (Celeb[])xf.Deserialize(fs);
                    foreach(Celeb c in p)
                        Console.WriteLine("Név: " + c.Nev + " IQ: " + c.Iq);
                }




                //létezik ilyen, ami az egész alattunk levőt helyettesíi egy sorral
                //cw-ben File.ReadAllText(FILENAME);
                //ekkor le se kell zárni, ő maga megcsinál mindent, wow
                //StreamReader sr = new StreamReader("celebs.xml", e);
                //string s = "";
                //string seged = "";
                //int i = 0;
                ////sr.ReadLine();
                //int db = int.Parse(sr.ReadLine()); //le kéne csonkítani a végét.
                //while (!sr.EndOfStream)
                //{
                //    s = sr.ReadLine();
                //    Console.WriteLine(s);
                //    i = 0;

                //    while (char.IsLetter(s[i]))
                //    {
                //        seged += s[i];
                //        i++;
                //    }//spacere
                //    i++;//|re
                //    i++;//spacere
                //    i++;//köv intre

                //    //lecsekkoljuk jó nevet kaptunk e meg
                //    //jó név: első betű nagy betű, többi kicsi



                //        Celeb c = new Celeb(seged);
                //        seged = "";
                //        for (; i < s.Length; i++)
                //        {
                //            seged += s[i];
                //        }
                //        c.Iq = int.Parse(seged);
                //        lista.Add(c);

                //    seged = "";
                //}

                //sr.Dispose();
            }
            else if (File.Exists("celebs.bin"))
            {

                using (FileStream fs = File.Open("celebs.bin", FileMode.Open))
                {
                    Celeb[] p = bf.Deserialize(fs) as Celeb[];
                    foreach (Celeb c in p)
                        Console.WriteLine("Név: " + c.Nev + " IQ: " + c.Iq);
                }



                //létezik ilyen, ami az egész alattunk levőt helyettesíi egy sorral
                //cw-ben File.ReadAllText(FILENAME);
                //ekkor le se kell zárni, ő maga megcsinál mindent, wow
                //StreamReader sr = new StreamReader("celebs.bin", e);
                //string s = "";
                //string seged = "";
                //int i = 0;
                ////sr.ReadLine();
                //int db = int.Parse(sr.ReadLine()); //le kéne csonkítani a végét.
                //while (!sr.EndOfStream)
                //{
                //    s = sr.ReadLine();
                //    Console.WriteLine(s);
                //    i = 0;

                //    while (char.IsLetter(s[i]))
                //    {
                //        seged += s[i];
                //        i++;
                //    }//spacere
                //    i++;//|re
                //    i++;//spacere
                //    i++;//köv intre

                //    //lecsekkoljuk jó nevet kaptunk e meg
                //    //jó név: első betű nagy betű, többi kicsi



                //    Celeb c = new Celeb(seged);
                //    seged = "";
                //    for (; i < s.Length; i++)
                //    {
                //        seged += s[i];
                //    }
                //    c.Iq = int.Parse(seged);
                //    lista.Add(c);

                //    seged = "";
                //}

                //sr.Dispose();
            }
            else//kérjünk be celeb neveket a consolból, azokból csináljunk celebeket és a nevüket írjuk ki a most létrehozott fájlba
            {
                //kérdezzünk rá, hogy xml, vagy bináris sorosítást akar-e a felhasználó

                Console.WriteLine("XML vagy BIN sorosítás legyen?");
                string sorositastipus = Console.ReadLine();
                string celebfilename = "";
                if (sorositastipus == "XML")
                {
                    //celebs.xml legyen
                    celebfilename = "celebs.xml";
                }
                else if (sorositastipus == "BIN")
                {
                    //celebs.bin legyen
                    celebfilename = "celebs.bin";
                }
                else
                {
                    Console.WriteLine("Hibásat adott meg. Bin lesz.");
                    celebfilename = "celebs.bin";
                }

                Celeb[] tomb = new Celeb[4];

                string regexname = @"^([A-ZÉÁŐÚŰÖÜÓ][a-zéáőúűöüó]*)(( |-)?([A-ZÉÁŐÚŰÖÜÓ][a-zéáőúűöüó]*)){0,4}";
                string seged = "";
                //StreamWriter sw = new StreamWriter(celebfilename, false, e);

                for (int i = 0; i < tomb.Length; i++)
                {
                    Console.WriteLine("Adja meg a következő celeb nevét:");
                    seged = Console.ReadLine();
                    if (!Regex.IsMatch(seged, regexname))
                    {
                        seged = "Kata";
                        Console.WriteLine("Rossz név lett megadva, alap celeb készült Kata néven.");
                    }
                    tomb[i] = new Celeb(seged);
                }
                
                if (sorositastipus == "XML")
                {

                    using (FileStream fs = File.Create(celebfilename))
                    {
                        xf.Serialize(fs, tomb);
                    }

                }
                else//BIN
                {

                    using (FileStream fs = File.Create(celebfilename))
                    {
                        bf.Serialize(fs, tomb);
                    }
                }


                //sw.WriteLine("4");
                //sw.WriteLine(c1.Nev + " | " + c1.Iq);
                //sw.WriteLine(c2.Nev + " | " + c2.Iq);
                //sw.WriteLine(c3.Nev + " | " + c3.Iq);
                //sw.WriteLine(c4.Nev + " | " + c4.Iq);

                //sw.Close();
            }

            //foreach (Celeb c in lista)
            //{
            //    Console.WriteLine(c.Nev + " " + c.Iq);
            //}

            Console.ReadLine();
        }
    }
}
