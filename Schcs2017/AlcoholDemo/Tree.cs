﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlcoholDemo
{
    class Tree : IBurnable, IBurnable2
    {
        private int _temperature;
        public int Temperature
        {
            get
            {
                return _temperature;
            }
            set//ez nem kötelező
            {
                _temperature = value;
            }
        }

        public void Burn()
        {
            Console.WriteLine("Ég a fa");
        }

        void IBurnable2.Burn()
        {
            Console.WriteLine("Ég a fa Kettő!");
        }
    }
}
