﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebLayer.DTO
{
    public class Dept
    {
        public int deptno { get; set; }
        public string dname { get; set; }
        public string loc { get; set; }
    }
}
//camel case formatter, emiatt kezdődjenek kisbetűkkel ezek.