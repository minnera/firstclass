﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace wpf2_1_nettv
{
    class ListWindowViewModel : Bindable
    {
        private List<Order> items;

        public List<Order> Items
        {
            get { return items; }
            set { SetValue(ref items, value); }
        }
    }
}
