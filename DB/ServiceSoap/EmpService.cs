﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using AutoMapper;
using BusinessLogic;

//implementálja a szolgáltatást
namespace ServiceSoap
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in both code and config file together.
    public class EmpService : IEmpService
    {
        //public string GetData(int value)
        //{
        //    return string.Format("You entered: {0}", value);
        //}

        //public CompositeType GetDataUsingDataContract(CompositeType composite)
        //{
        //    if (composite == null)
        //    {
        //        throw new ArgumentNullException("composite");
        //    }
        //    if (composite.BoolValue)
        //    {
        //        composite.StringValue += "Suffix";
        //    }
        //    return composite;
        //}

        private ILogic logic;
        private IMapper mapper;

        public EmpService()
        {
            logic = new RealLogic();
            mapper = DTO.AutoMapperConfig.GetMapper();
        }

        public DTO.Emp GetWorkerById(int id)
        {
            Data.EMP worker = logic.GetOneWorker(id);
            return mapper.Map<Data.EMP, DTO.Emp>(worker);
        }

        public List<DTO.Average> GetWorkerAverages()
        {
            IQueryable<BusinessLogic.GetAveragesResult> list = logic.GetAverages();
            return mapper.Map<IQueryable<BusinessLogic.GetAveragesResult>, List<DTO.Average>>(list);
        }
    }
}
