﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using WpfApplication2_mvvm.Models;

namespace WpfApplication2_mvvm.ViewModel
{
    public interface IMainViewLogic
    {
        void AddCmd();
        void DelCmd(DeptVM d);
        void ModCmd(DeptVM d);
        void ChangeEnabledCmd(MouseEventArgs e);
        void UpdateViaJsonCmd();

        ObservableCollection<DeptVM> GetDeptList();

    }
}
//public legyen
//itt megmondjuk konkrétan hogy a metódusok milyenek, paraméterek, visszatérés, stb