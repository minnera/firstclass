﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryDemo
{
    class Kitchen
    {
        public static Food Give(Human h)
        {
            if (h.IsVegetarian)
            {
                var result = new PlantFood();
                if (h.Age < 10)
                {
                    result.Type = "Bébiétel";
                }
                else
                {
                    result.Type = "Brokkoli";
                }
                return result;
            }else
            {
                var result = new AnimalFood();
                if (h.Age < 10)
                {
                    result.Type = "Bébikaja";
                }
                else
                {
                    result.Type = "Steak";
                }
                return result;
            }
        }
    }
}