﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data;
using Repository;
using Repository.DeptRepos;
using Repository.EmpRepos;

namespace BusinessLogic
{
    public class FakeLogic : ILogic
    {
        private MyRepository Repo;

        public FakeLogic()
        {
            EmpListRepository er = new EmpListRepository();
            DeptListRepository dr = new DeptListRepository();
            Repo = new MyRepository(er, dr);
        }

        public IQueryable<GetAveragesResult> GetAverages()
        {
            //itt akarunk tesztelni, van e hiba?
            //megmondjuk ilyen bemenetre ilyen kimenet kell és teszteljük

            //-re -kimenet
            List<GetAveragesResult> output = new List<GetAveragesResult>();
            output.Add(new GetAveragesResult()
            {
                Job = "ANALYST", Avg = 3000
            });
            output.Add(new GetAveragesResult()
            {
                Job = "CLERK",
                Avg = 1037.5m //ha mögé rakod az f-et = float, m-et = decimal, simán a ###.## szám double lesz.
            });
            output.Add(new GetAveragesResult()
            {
                Job = "MANAGER",
                Avg = 2758.333333m
            });
            output.Add(new GetAveragesResult()
            {
                Job = "PRESIDENT",
                Avg = 5000
            });
            output.Add(new GetAveragesResult()
            {
                Job = "SALESMAN",
                Avg = 1400
            });
            return output.AsQueryable();
        }

        public IQueryable<EMP> GetWorkers()
        {
            return Repo.empRepo.GetAll();
        }

        public IQueryable<DEPT> GetDepartments()
        {
            return Repo.deptRepo.GetAll();
        }

        public EMP GetOneWorker(int id)
        {
            return Repo.empRepo.GetById(id);
        }

        public void AddDept(DEPT newdept)
        {
            Repo.deptRepo.Insert(newdept);
        }

        public void DelDept(int id)
        {
            Repo.deptRepo.Delete(id);
        }

        public void ModifyDept(int id, string name, string loc)
        {
            Repo.deptRepo.Modify(id, name, loc);
        }

        public DEPT GetOneDept(int id)
        {
            return Repo.deptRepo.GetById(id);
        }

        public int GetNextDeptno()
        {
            return Repo.deptRepo.GetAll().Max(x => (int)x.DEPTNO) + 10;
        }
    }
}
