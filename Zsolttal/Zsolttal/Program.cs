﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zsolttal
{
    class Program
    {
        static void Main(string[] args)
        {
            //ezt mindig csekkolni kell
            //most nem egy fizikai szerverhez csatlakozunk csak egy fájlhoz.
            //(LocalDB)\v11.0
            string connStr = @"data source = (LocalDB)\MSSQLLocalDB; attachdbfilename = |DataDirectory|\CarDb.mdf; integrated security = true";
            //ebben nem csak csatlakozási adatok lehetnek hanem config adatok is
            
            //Project / Add new Item / Data / ADO.NET Entity Data model / EF Designer From Database <NEXT>

            MyProvider provider = new MyProvider(connStr);

            Console.WriteLine("OK");
            foreach (var row in provider.ExecSelect("SELECT * FROM emp"))
            {
                Console.WriteLine(row["ENAME"]);
            }
            Console.WriteLine("DONE");
            Console.WriteLine(provider.ExecGetName(7788));

            string sql = "DELETE FROM emp WHERE empno = @empno ";
            Dictionary<string, string> sqlParams = new Dictionary<string, string>();
            sqlParams.Add("@empno","7369");

            int affected = provider.ExecNonSelect(sql, sqlParams);
            Console.WriteLine("DELETED: " + affected);
            Console.WriteLine(provider.ExecGetName(7369));

            try
            {
                sqlParams.Clear();

                sqlParams.Add("@empno", "7788");
                provider.ExecNonSelect(sql, sqlParams);
                Console.WriteLine("DELETED");
            }
            catch (SqlException ex)
            {
                for (int i = 0; i < ex.Errors.Count; i++)
                {
                    Console.WriteLine("Error #{0}: [{2}]: {1}", i, ex.Errors[i].Message, ex.Errors[i].Number);
                }
            }

            //ilyenkor a rendes táblánkból nem töröljük, így minden ujrainditásnál újra ott van

            Console.ReadLine();
        }
    }
}
