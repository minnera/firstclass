﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic;
using Data;
using Repository;
using Repository.DeptRepos;
using Repository.EmpRepos;

namespace DB
{
    class Program
    {
        static void Main(string[] args)
        {
            EmpDeptEntities ED = new EmpDeptEntities();

            foreach (var akt in ED.EMP)
            {
                Console.WriteLine(akt.ENAME);
            }

            //IQueryable // vagyis később kezdődik a lekérdezés maga, ezért nem lehet metodust irni bele simán
            //késleltetett a végrehajtódása, optimized
            var q1 = from akt in ED.EMP
                     group akt by akt.JOB into g
                     select new { JOB = g.Key, NUM = g.Count(), AVG = g.Average(x => x.SAL + (x.COMM ?? 0) )};
            //select new { JOB = g.Key, NUM = g.Count(), AVG = g.Average(x => x.SAL + x.COMM.GetValueOrDefault())};
            //ez nem jó


            //listán használt linq nál bármilyen methpdot lehet használni,
            //adatbáhzis linq-nál már nem lehet bármit használni
            //ilyenkor csak ilyet lehet használni:
            //System.Data.Entity.Core.Objects.EntityFunctions.

            foreach (var akt in q1)
            {
                Console.WriteLine(akt);
            }

            Console.ReadLine();

            //csatolni szeretnénk, JOIN kulcsszóra példa
            var q2 = from akt in ED.EMP
                     join data in q1 on akt.JOB equals data.JOB//itt újra lekérdezi a q1 eredményt
                     select new { akt.ENAME, akt.JOB, data.AVG, akt.DEPT.DNAME};

            //nem tárol el lekérdezéseket, mindig újra csinálja meg őket

            //ha így csináljuk a lekérdezéseket, akkor megszűnik az sql injektion veszélye is,
            //mert egyértelmű, hogy mi az adat,
            //és az adatok is el vannak rejtve
            //+csekkolja is a códot fordítás előtt, nincs elírási hiba, mint stinrgből sqlnél

            foreach (var akt in q2)
            {
                Console.WriteLine(akt);
            }


            /*
                EMP newWorker = new EMP();
                newWorker.EMPNO = 777;
                ED.EMP.Add(newWorker);
                Ed.SaveChanges();

                EMP oldWorker = ED.EMP.Single(x => x.EMPNO == 777);
                ED.EMP.Remove(oldWorker);
                Ed.SaveChanges();
            */

            Console.ReadLine();

            IEmpRepository er = new EmpEFRepository(ED);
            IDeptRepository dr = new DeptListRepository();

            MyRepository repo = new MyRepository(er, dr);

            foreach (var akt in repo.empRepo.GetAll())
            {
                Console.WriteLine(akt.ENAME);
            }
            foreach (var akt in repo.deptRepo.GetAll())
            {
                Console.WriteLine(akt.DNAME);
            }

            Console.ReadLine();

            ILogic BL = new RealLogic(repo);
            foreach(var akt in BL.GetAverages()) Console.WriteLine(akt);

            Console.ReadLine();

            BL = new FakeLogic();
            foreach (var akt in BL.GetAverages()) Console.WriteLine(akt);

            Console.ReadLine();
        }
    }
}
