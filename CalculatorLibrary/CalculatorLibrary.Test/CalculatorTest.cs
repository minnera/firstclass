﻿using System;
using NUnit.Framework;

//először Build
//ezután test explorer-ben megjelenik a test
//ezután ott lehet futtatni run vagy run all


namespace CalculatorLibrary.Test
{
    [TestFixture]
    public class CalculatorTest
    {
        //[Test]
        [TestCase(1,2,3)]
        [TestCase(10,-10,0)]
        public void AddShouldReturnExpectedValueOnCall(int a, int b, int expected)
        {
            //a tesztek 3 lépésből szoktak állni:
            
            //arrange
            //megteremted a megfelelő környezetet, hogy a teszt futhasson
            Calculator c = new Calculator();//hogy ezt lássuk, a calculatorlibraryt referencbe kellett adni a testnél
            

            //act
            //itt tesztelünk

            int result = c.Add(a, b);


            //assert
            //megnézzük, hogy ez tényleg annyi-e

            Assert.AreEqual(expected, result);

        }

        //nunit-nál nem kell így különvenni a teszteket, ezért ezt kikommentezzük
        //ezt elrontottam, eg ymetódusra nem kell több ilyet írni

        //[Test]
        //public void RemoveShouldReturn1For2And1()
        //{
        //    //a tesztek 3 lépésből szoktak állni:

        //    //arrange
        //    //megteremted a megfelelő környezetet, hogy a teszt futhasson
        //    Calculator c = new Calculator();//hogy ezt lássuk, a calculatorlibraryt referencbe kellett adni a testnél

        //    int a = 2;
        //    int b = 1;

        //    //act
        //    //itt tesztelünk

        //    int result = c.Remove(a, b);


        //    //assert
        //    //megnézzük, hogy ez tényleg annyi-e

        //    Assert.AreEqual(1, result);

        //}
    }
}
