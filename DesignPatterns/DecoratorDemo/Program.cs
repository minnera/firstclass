﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DecoratorDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            Fegyver f = new Puska();
            f.Tuzeles();

            Fegyver f2 = new Tavcso(new Puska());
            f2.Tuzeles();

            Fegyver f3 = new Granatveto(new Tavcso(new Golyoszoro()));
            f3.Tuzeles();

            Console.ReadLine();
        }
    }
}
