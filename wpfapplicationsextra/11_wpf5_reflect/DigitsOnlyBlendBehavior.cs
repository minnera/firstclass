﻿using System;
using System.Linq;
using System.Windows.Controls;
using System.Windows.Interactivity;

namespace _11_wpf5_reflect
{
    class DigitsOnlyBlendBehavior : Behavior<TextBox>
    {
        protected override void OnAttached()
        {
            base.OnAttached();
            AssociatedObject.PreviewTextInput += AssociatedObject_PreviewTextInput;
            //ezeket az eseménykezelőket mindig tab-bal kell készíeni, autogenerálni
        }

        private void AssociatedObject_PreviewTextInput(object sender, System.Windows.Input.TextCompositionEventArgs e)
        {
            e.Handled = e.Text.Any(c => !Char.IsDigit(c));
        }

        protected override void OnDetaching()
        {
            base.OnDetaching();
            AssociatedObject.PreviewTextInput -= AssociatedObject_PreviewTextInput;
        }
    }
}
