﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoGUI
{
    [AttributeUsage(AttributeTargets.Property)]//ezt az attributomot csak tulajdonságra lehet ráhelyezni
    public class NoAutoGuiAttribute : Attribute
    {
        public string Reason { get; private set; }

        public NoAutoGuiAttribute(string newreason)
        {
            Reason = newreason;
        }
    }
}
