﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace EncodingDemo
{
    class Program
    {
        const string FILENAME = "test.txt";
        const string SZOVEG = "árvíztűrő tükörfúrógép";
        static void Main(string[] args)
        {
            DisplayEncoding();

            Console.WriteLine("Add meg a kódlap számát: ");

            int codepage = int.Parse(Console.ReadLine());//encodinginfo.codepage szám ez
            Encoding e = Encoding.GetEncoding(codepage);

            if (File.Exists(FILENAME))
            {
                Console.WriteLine("{0} beolvasása", FILENAME);
                using(StreamReader sr = new StreamReader(FILENAME, e))
                {
                    Console.WriteLine(sr.ReadToEnd());
                }
            }
            else
            {
                Console.WriteLine("{0} létrehozása", FILENAME);
                using(StreamWriter sw = new StreamWriter(FILENAME, false, e))
                {
                    sw.WriteLine(SZOVEG);
                }
            }

            Console.ReadLine();
        }

        private static void DisplayEncoding()
        {
            //vagy létrehozunk egy fájlt, vagy felolvasunk egy fájlt, ha már létezik
            foreach (EncodingInfo info in Encoding.GetEncodings())
            {
                Console.WriteLine("Név: {0},\tkódlap: {1}", info.Name, info.CodePage);
            }
        }
    }
}
