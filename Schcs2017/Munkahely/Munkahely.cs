﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Munkahely
{
    enum Melosok
    {
        Fejleszt, Tesztel, Tervez
    };
    class Munkahely
    {
        private List<Melos> emberek;

        internal List<Melos> Emberek
        {
            get
            {
                return emberek;
            }

            set
            {
                emberek = value;
            }
        }

        public Munkahely()
        {
            emberek = new List<Melos>();
        }

        public void MelostKap(Melos m)
        {
            emberek.Add(m);
        }

        public void Dolgoztat(Munka doit)
        {
            foreach (Melos m in emberek)
            {
                if (m is Bela && doit.MunkaJellege == "Tesztel")
                {
                    ((Bela)m).Tesztel(doit);
                    return;
                }
                else if (m is Tamas && doit.MunkaJellege == "Fejleszt")
                {
                    ((Tamas)m).Fejleszt(doit);
                    return;
                }
                else if (m is Gergely && doit.MunkaJellege == "Tervez")
                {
                    ((Gergely)m).Tervez(doit);
                    return;
                }
            }
            Console.WriteLine("A munka nem készült el, nem volt rá ember.");
        }

    }
}
