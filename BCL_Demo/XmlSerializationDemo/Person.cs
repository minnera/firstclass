﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;//ez kell

using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace XmlSerializationDemo
{
    
    public class Person//ennek publicnak kell lennie, különben nem sorosíthatjuk
    {
        private string _name;

        public string Name
        {
            get
            {
                return _name;
            }

            set
            {
                _name = value;
            }
        }

        private DateTime _birtdate;

        public DateTime Birtdate
        {
            get
            {
                return _birtdate;
            }

            set
            {
                _birtdate = value;
            }
        }
        
        private int _age;
        //itt sml sorosításnál ezzel lehet egy tulajdonságot letiltani
        //itt csak tulajdonságot lehet, korábban csak mezőt lehetett
        [XmlIgnore]
        public int Age
        {
            get
            {
                return _age;
            }

            set
            {
                _age = value;
            }
        }

        public Person(string name, DateTime birth, int age)
        {
            _name = name;
            _birtdate = birth;
            _age = age;
        }
        //és hogy sorosítható legyen xmlként kell egy üres construktor is a public osztály mellett
        public Person()
        {

        }
        
    }
}
