﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            Human h1 = new Human() {Age = 20, IsVegetarian = true};
            Human h2 = new Human() { Age = 8, IsVegetarian = false };

            Food f1 = Kitchen.Give(h1);
            h1.Eat(f1);
            Food f2 = Kitchen.Give(h2);
            h2.Eat(f2);

            Console.ReadLine();
        }
    }
}
