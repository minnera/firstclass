﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Security.AccessControl;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using AutoMapper;
using BusinessLogic;
using GalaSoft.MvvmLight.Messaging;
using Microsoft.Practices.ServiceLocation;
using Newtonsoft.Json;
using WpfApplication2_mvvm.Models;
using WpfApplication2_mvvm.Services;

namespace WpfApplication2_mvvm.ViewModel
{
    public class MainViewLogic : IMainViewLogic
    {

        ILogic BusinessLogic
        {
            get { return ServiceLocator.Current.GetInstance<ILogic>(); }
        }

        IDeptEditorService Editor
        {
            get { return ServiceLocator.Current.GetInstance<IDeptEditorService>(); }
        }

        IMainViewModel VM
        {
            get { return ServiceLocator.Current.GetInstance<IMainViewModel>(); }
        }

        IMapper Mapper
        {
            get { return ServiceLocator.Current.GetInstance<IMapper>(); }
        }

        public void AddCmd()
        {
            DeptVM dept = new DeptVM();
            if (Editor.EditDept(dept))
            {
                dept.Deptno = BusinessLogic.GetNextDeptno();
                Data.DEPT dbDept = Mapper.Map<DeptVM, Data.DEPT>(dept);
                BusinessLogic.AddDept(dbDept);
                VM.Depts.Add(dept);//db content <=> listbox item
            }
        }

        public void DelCmd(DeptVM d)
        {
            if (d == null)
            {
                //ekkor nem tudunk törölni
                Messenger.Default.Send(new NotificationMessage("No dept selected!"));
                return;
            }
            BusinessLogic.DelDept(d.Deptno);
            VM.Depts.Remove(d);
            
        }

        public void ModCmd(DeptVM d)
        {
            if (d == null)
            {
                Messenger.Default.Send(new NotificationMessage("No dept selected!"));
                return;
            }

            DeptVM clone = d.Clone();
            if (Editor.EditDept(clone))
            {
                BusinessLogic.ModifyDept(d.Deptno, clone.Dname, clone.Loc);
                d.CopyDataFrom(clone);
            }
        }

        public void ChangeEnabledCmd(MouseEventArgs e)
        {
            //nullreferenc
            if ((e.OriginalSource as FrameworkElement)?.Tag?.ToString() == "EnabledBlock")
            {
                VM.SelectedDept.Enabled = !VM.SelectedDept.Enabled;
            }
        }

        public void UpdateViaJsonCmd()
        {
            Messenger.Default.Send(new NotificationMessage("Downloading"));
            string url = "http://localhost:18758/api/test/depts";
            string json = new System.Net.WebClient().DownloadString(url);
            Messenger.Default.Send(new NotificationMessage(json));
            var list = JsonConvert.DeserializeObject<List<DeptDTO>>(json);
            foreach (var v in list)
            {
                if (VM.Depts.Any(x => x.Deptno == v.deptno))
                {
                    BusinessLogic.ModifyDept(v.deptno, v.dname, v.loc);
                    Messenger.Default.Send(new NotificationMessage("UPD: " + v.deptno));
                }
                else
                {
                    Data.DEPT dbDept = Mapper.Map<DeptDTO, Data.DEPT>(v);
                    BusinessLogic.AddDept(dbDept);
                    Messenger.Default.Send(new NotificationMessage("INS: " + v.deptno));
                }
            }
            VM.Depts = GetDeptList();
        }

        public ObservableCollection<DeptVM> GetDeptList()
        {
            var deptsDB = BusinessLogic.GetDepartments().ToList();
            return Mapper.Map<List<Data.DEPT>, ObservableCollection<DeptVM>>(deptsDB);
        }
    }
}
//public
//ez lesz a fő ablak, emellett lesz egy másik ablak is