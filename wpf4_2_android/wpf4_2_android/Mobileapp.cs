﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace wpf4_2_android
{
    class MobileApp : Bindable
    {
        private string name;
        private DateTime creationDate;
        private string author;
        private int size;

        public string Name
        {
            get { return name; }
            set { SetValue(ref name, value); }
        }

        public DateTime CreationDate
        {
            get { return creationDate; }
            set { SetValue(ref creationDate, value); }
        }

        public string Author
        {
            get { return author; }
            set { SetValue(ref author, value); }
        }

        public int Size
        {
            get { return size; }
            set { SetValue(ref size, value); }
        }
    }
}
