﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlcoholDemo
{
    interface IBurnable
    {
        void Burn();
        
        int Temperature { get;}
    }

    interface IBurnable2
    {
        void Burn();
    }

    interface IVeryBurnable : IBurnable
    {
        void Burn();
        int Temperature { get; }
        void BurnVery();
    }
}
