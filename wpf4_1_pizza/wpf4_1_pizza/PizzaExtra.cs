﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace wpf4_1_pizza
{
    class PizzaExtra : Bindable
    {
        private int price;
        private string name;
        private bool isWanted;

        public int Price
        {
            get { return price; }
            set { SetValue(ref price, value); }
        }

        public string Name
        {
            get { return name; }
            set { SetValue(ref name, value); }
        }

        public bool IsWanted
        {
            get { return isWanted; }
            set { SetValue(ref isWanted, value); }
        }
    }
}
