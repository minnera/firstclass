﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            Human c = new Human();
            c.Name = "Cartman";
            Human c2 = new Human();
            c2.Name = "Stan";

            Alien a1 = new Alien();
            Alien a2 = new Alien();
            Jewsian j1 = new Jewsian();

            c.PulseChanged += a1.Spy;
            c.PulseChanged += a2.Spy;
            c2.PulseChanged += a1.Spy;
            c.Pulse++;
            c2.Pulse++;


            //c.PulseChanged += a1.Spy;
            //c.PulseChanged += a2.Spy;
            //c.Pulse++;

            //c.PulseChanged -= a2.Spy;
            //c.PulseChanged += j1.React;
            //c.Pulse++;

            Console.ReadLine();
        }
    }
}
