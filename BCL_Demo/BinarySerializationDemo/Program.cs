﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;//kell ez
using System.Runtime.Serialization.Formatters.Binary;//kell ez

namespace BinarySerializationDemo
{
    class Program
    {
        const string FILENAME = "person.txt";
        static void Main(string[] args)
        {
            BinaryFormatter bf = new BinaryFormatter();
            if (File.Exists(FILENAME))
            {
                using (FileStream fs = new FileStream(FILENAME, FileMode.Open))
                {
                    Person p = bf.Deserialize(fs) as Person;
                    Console.WriteLine("Név: " + p.Name + " Kor: " + p.Age + " Született: " + p.Birtdate);
                }
            }
            else
            {
                Person p = new Person("Cristal", DateTime.Now.Subtract(new TimeSpan(1000, 0, 0, 0)), 2);
                Console.WriteLine(p.Birtdate);
                using(FileStream fs = new FileStream(FILENAME, FileMode.Create))
                {
                    bf.Serialize(fs, p);
                }
            }

            Console.ReadLine();
        }
    }
}
