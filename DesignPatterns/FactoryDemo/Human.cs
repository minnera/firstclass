﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryDemo
{
    class Human
    {
        private int _age;

        private bool _isVegetarian;

        

        public int Age
        {
            get { return _age; }
            set { _age = value; }
        }

        public bool IsVegetarian
        {
            get { return _isVegetarian; }
            set { _isVegetarian = value; }
        }

        public void Eat(Food f)
        {
            Console.WriteLine("Megegszem ezt a fajta kaját : " + f.GetType().Name);
        }
    }
}
