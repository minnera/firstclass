﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DelegateDemo
{
    internal class StringMethods
    {
        

        public void PrintFirst(string s)
        {
            Console.WriteLine(s[0]);
        }

        public void PrintLast(string s)
        {
            Console.WriteLine(s[s.Length - 1]);
        }
    }
}