﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CelebDemo
{
    public delegate void VoidDelegate(Celeb c1);
    class Program
    {
        static void Main(string[] args)
        {
            TypeTest();

            Celeb lelegzoceleb = new OkosCeleb();
            Egysejtu ezisel = new Egysejtu();
            Kutato k = new Kutato();
            ezisel.MeretValtozott += k.Mikroszkop;
            ezisel.Meret++;


            ILegzokepes[] legzok = new ILegzokepes[] { lelegzoceleb, ezisel};
            legzok[0].Lelegzik();
            legzok[1].Lelegzik();

            Celeb uj = (Celeb)ezisel;
            Egysejtu megegy = new Egysejtu();
            
            Egysejtu eredmeny = ezisel + megegy;

            Console.WriteLine("A keletkezett celeb neve : {0} , IQ-ja: {1}.",uj.Nev, uj.Iq);

            Console.WriteLine("A keletkezett egysejtű mérete: {0}", eredmeny.Meret);

            Program p = new Program();
            VoidDelegate vd = p.Ez;
            uj.Tarsalog(lelegzoceleb, vd);
            uj.Tarsalog(lelegzoceleb, p.Az);

            CelebList<ButaCeleb> butasagok = new CelebList<ButaCeleb>();
            ButaCeleb b1 = new ButaCeleb("Sakura");
            ButaCeleb b2 = new ButaCeleb("Hori");
            butasagok.Add(b1);
            butasagok.Add(b2);
            string butanev = butasagok.Cast<string>(0, new CastTtoUDelegate<ButaCeleb, string>(CastButaCelebToString));
            Console.WriteLine(butanev);

            OkosCeleb[] okosok = new OkosCeleb[2] { new OkosCeleb("Sarolt"), new OkosCeleb("Írisz")};

            OkosCeleb[] rendezett = CelebRendezo.Rendezo<OkosCeleb, OkosCelebRendezo>(okosok, new OkosCelebRendezo());

            rendezett = CelebRendezo.Rendezo(okosok, delegate (OkosCeleb a, OkosCeleb b)
            {
                return a.Iq - b.Iq;
            });


            Console.WriteLine(rendezett[0].Nev);
            Console.WriteLine(rendezett[1].Nev);




            //Celeb[] tiltott = new Celeb[] { "Trump", "Pákó" };
            //int db = 3;
            //Celeb[] favorites = new Celeb[db];
            List<Celeb> favorites = new List<Celeb>();
            string seged = "";
            //for(int i = 0; i < db; i++)
            while(seged != "ELÉG")
            {
                Console.WriteLine("Adja meg a következő celeb nevét:");
                
                seged = Console.ReadLine();
                //van olyan hogy seged.Contains("Pako"), ami ha a seged tartalmazza a string részletet igazt ad
                //seged.ToUpper().Contains("PAKO")
                //Thread.CurrentThread.CurrentCulture.CompareInfo.Compare(seged, "PAKO", CompareOptions.IgnoreCase) == 0 , akkor egyeznek 
                /*if (seged == "Trump") // lehet seged.Equals("Trump")
                {
                    favorites[i].Nev = "Mr.Smith";
                }
                else {
                    favorites[i].Nev = seged;
                }*/

                ///////////////////////////////6

                /*string[] tiltottnevek = new string[] { "Pako", "Trump" };

                if (tiltottnevek.Contains(seged))
                {
                    seged = "defaultname";
                }*/

                //vagy csak csináld ugy hogy uj celeb objektum annak adod a nevet és azt adod a listához.
                string nevecske = "";


                
                switch (seged)
                {
                    case "ELÉG":
                        break;
                    case "Trump":
                    case "Kardashian":
                        nevecske = "Mr.Smith";//favorites[favorites.Count - 1].Nev = "";
                        break;
                    default:
                        nevecske = seged;
                        break;
                }
                if (nevecske != "")
                    favorites.Add(new ButaCeleb(nevecske));
                else if(seged != "ELÉG")
                    favorites.Add(new ButaCeleb(seged));
            }

            Console.WriteLine("Kiiratás:");
            //lehetne foreach is
            for (int i = 0; i < favorites.Count; i++)
            {
                Console.WriteLine(favorites[i].Nev + ", " + favorites[i].Iq);
                //Console.WriteLine("Nev: {0}, IQ: {1}.", favorites[i].Nev, favorites[i].Iq);
                //tanár szerint ez a szebb
            }
            Console.WriteLine("Tanítás:");
            foreach (Celeb ez in favorites)
            {
                ez.Tanit();
                Console.WriteLine(ez.Nev + ", " + ez.Iq);
            }
            Celeb c1 = new ButaCeleb("Trump");
            Celeb c2 = new ButaCeleb("Kitty");
            c1.Iq = 10;
            c2.Iq = -2;
            Celeb butaaa = new ButaCeleb();
            butaaa = Celeb.Legbutabb(c1, c2, Celeb.Alapertelmezett(3));
            Console.WriteLine("Legbutább: ");
            Console.WriteLine(butaaa.Nev + ", iq: " + butaaa.Iq);
            Console.ReadKey();
        }

        private static string CastButaCelebToString(ButaCeleb t)
        {
            return t.Nev;
        }

        private static void TypeTest()
        {
            object o = 42;
            if (o is int)
            {
                int i = (int)o + 10;
                Console.WriteLine(i);
            }
            //de le kell csekkolnunk, hogy tényleg int van e ott, hogy a hibát elkerüljük.
            //egyik lehetőség a kivétel elkapása
            //másik tesztelés átalakítás előtt
        }

        public void Ez(Celeb a)
        {
            Console.WriteLine(a.Nev);
        }

        public void Az(Celeb b)
        {
            Console.WriteLine(b.Iq);
        }

        public void Emez(Celeb a)
        {
            Console.WriteLine(a.Nev + " és " + a.Iq);
        }
    }
}
