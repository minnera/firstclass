﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OperatorsDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            //int i = 8;
            //double d = 9.9;
            //d = i;//itt valójában történik egy konverzió, egy bővítő konverzió. ez inplicit, ezért nem kell kiírni, hogy (double), de kilehet
            //i = (int)d;// ilyet viszont nem lehet, ez egy szűkítő konverzió. Itt mindig ki kell írni, hogy konvertálod, explicit ez.

            //int j = 9;
            //int k = i + j;
            //k++;

            Pill p1 = new Pill("Vörös");
            Wine w1 = (Wine)p1; // de lehet majd simán () nélkül is.
            

            Console.WriteLine(w1.Type);
            Console.WriteLine(w1.Percent);

            w1++;
            Console.WriteLine(w1.Percent);

            Wine w2 = new Wine("fehér", 12.0);

            Wine w3 = w1 + w2;
            Console.WriteLine(w3.Type + " " + w3.Percent);



            Console.ReadLine();
        }
    }
}
