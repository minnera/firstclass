﻿using System.Windows;
using System.Windows.Interactivity;
using System.Windows.Media;

namespace _11_wpf5_reflect
{
    class DragBehavior : Behavior<FrameworkElement>
    {
        //mozgatás
        //LayoutTransform, RenderTransform
        //első: elrendezés előtt, másik: elrendezés után fut le
        //a lt. hatással lehet más elemekre is, míg a másiknál csak mozgatsz (nincsen hatással a többi elemre)és így átfedések alakulhatnak ki.
        private Point mouseStartPoint;
        private TranslateTransform currentTransform;
        private TranslateTransform initialTransform;
        private Window parent;

        protected override void OnAttached()
        {
            //ezt soha nem javasolt ahsználni,mert innentől kezdve csak a fő ablakon fog működni.
            //parent = Application.Current.MainWindow;

            parent = Window.GetWindow(AssociatedObject);
            currentTransform = new TranslateTransform();
            AssociatedObject.RenderTransform = currentTransform;

            //először azt csináljuk, hogy csak a jobb gombbal rángassuk
            AssociatedObject.PreviewMouseRightButtonDown += MouseDown;
            AssociatedObject.PreviewMouseRightButtonUp += MouseUp;
            AssociatedObject.PreviewMouseMove += MouseMove;
        }

        private void MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            mouseStartPoint = e.GetPosition(parent);
            initialTransform = new TranslateTransform(currentTransform.X, currentTransform.Y);
            AssociatedObject.CaptureMouse();
        }

        private void MouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            AssociatedObject.ReleaseMouseCapture();
        }

        private void MouseMove(object sender, System.Windows.Input.MouseEventArgs e)
        {
            if (AssociatedObject.IsMouseCaptured && initialTransform != null)
            {
                Vector diff = e.GetPosition(parent) - mouseStartPoint; //aktuális pozíció - kezdőpozíció
                currentTransform.X = diff.X + initialTransform.X;
                currentTransform.Y = diff.Y + initialTransform.Y;
            }
        }
    }
}
