﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace wpf4_1_pizza
{
    //az ablakkal milyen interakciók lehetnek kb azt íja le ez az interface
    interface IMainViewModel : INotifyPropertyChanged
    {
        string Address { get; }
        Pizza Pizza { get; }
        //ha ienumerable: akkor nem lehet hozzáadni majd már
        //ha .... akkor még lehet hozzáadni utólag elemeket
        IEnumerable<PizzaExtra> Sizes { get; } 
        ICommand SelectAll { get; }
    }
}
