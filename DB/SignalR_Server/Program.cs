﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using BusinessLogic;
using Microsoft.AspNet.SignalR;
using Microsoft.Owin;
using Microsoft.Owin.Cors;
using Microsoft.Owin.Hosting;
using Owin;

[assembly: OwinStartupAttribute(typeof(SignalR_Server.StartUp))]
namespace SignalR_Server
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Clear();
            Console.WriteLine("PRESS ENTER TO START THE SERVER");
            Console.ReadLine();

            string url = "http://localhost:8080";
            using (WebApp.Start(url))
            {
                Console.WriteLine("SERVER RUNNIING");
                Console.ReadLine();
            }
        }
    }
    //amúgy egy osztály egy fájl
    public class StartUp
    {
        public void Configuration(IAppBuilder app)
        {
            app.UseCors(CorsOptions.AllowAll);
            app.MapSignalR();
        }
    }

    public class Dept
    {
        public int deptno { get; set; }
        public string dname { get; set; }
        public string loc { get; set; }

        public override string ToString()
        {
            return String.Format("[{0}] {1} at {2}", deptno, dname, loc);
        }
    }

    public class Resources
    {
        private static Resources instance;
        public static Resources Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Resources();
                    instance.Logic = new RealLogic();
                    instance.Mapper = new MapperConfiguration(cfg =>
                    {
                        cfg.CreateMap<Dept, Data.DEPT>().ReverseMap();
                    }).CreateMapper();
                    
                }
                return instance;
            }
        }

        public IMapper Mapper { get; set; }
        public ILogic Logic { get; set; }

        private Resources()
        {
        }
    }

    public class DeptHub : Hub
    {
        public void DelDept(int id)
        {
            Resources.Instance.Logic.DelDept(id);
            Clients.All.Refresh();
        }

        public void AddDept(Dept d)
        {
            Data.DEPT realDept = Resources.Instance.Mapper.Map<Dept, Data.DEPT>(d);
            Resources.Instance.Logic.AddDept(realDept);
            Clients.All.DeptAdded(d);
        }

        public IEnumerable<Dept> GetDepts()
        {
            var list = Resources.Instance.Logic.GetDepartments();
            return Resources.Instance.Mapper.Map<IQueryable<Data.DEPT>, IEnumerable<Dept>>(list);
        }
    }
}
