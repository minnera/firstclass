﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenericsDemo
{
    //megkötéseket rakhatunk a típusparaméterre itt!
    class Papir<T> //ezzel a kacsacsőrös T-vel jelezzük, hogy várnia kell egy típust, ez a típus bármi lehet.
        where T : IComparable<T> 
    {
        private T _x;

        public T X
        {
            get
            {
                return _x;
            }

            set
            {
                _x = value;
            }
        }

        public T Y
        {
            get
            {
                return _y;
            }

            set
            {
                _y = value;
            }
        }

        private T _y;

        public T GetSmaller()
        {
            ////if (T is IComparable<T>)  // ilyet sajnos nem tud
            //if(typeof(T) is IComparable<T>) //ezt már lehet, de nem olyan szép.
            //    //de nekünk az kell, hogy már meg se lehessen hívni olyanra, aminél ez false-t adna, és nem csak itt rájönni.
            //{
            //}
            //return _x.CompareTo(_y) < 0 ? _x : _y;// ekkor ha egyenlő, akkor is y-t ad

            int result = _x.CompareTo(_y);
            if(result > 0)
            {
                return _y;
            }
            if(result < 0)
            {
                return _x;
            }
            //return null; //ami nem referencia típus az nem kaphat nullt-t!
            //el kéne tudni dönteni, hogy most referencia típus van vagy nem, de ez is elég lassú
            //erre van a default operátor, ez mindig jó alapértéket ad vissza.
            return default(T);
        }

        public T GetSmallerUsingAMethod(CompareDelegate<T> d)
        {
            return d(this._x, this._y);
        }

        //adjuk vissza az x-et vagy y-t egy másik típusként
        public U Cast<U>(CastTtoUDelegate<T, U> d)
        {
            T t = this.GetSmaller();
            U result = d(t);
            return result;
        }
    }

    delegate T CompareDelegate<T>(T a, T b);
    //delegate T CompareDelegate<T>(Papir<T> t) where T : IComparable<T>; //így is lehetne, de ekkor ugyanazokat a megkötéseket rá kell rakni erre is, mint amik a Papiron vannak
    delegate U CastTtoUDelegate<T, U>(T a);
}
