﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParametersDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] tomb = new string[] { "fészek","alma","béka", "cékla", "dézsa"};
            //string s = String.Empty;//ez jobb mint a ="", mert így eggyel kevesebb string áthelyezés kell a memoriában
            //foreach (string seged in tomb)
            //{
            //    s += seged;
            //    s += " ";
            //}
            //s = s.Substring(0, s.Length-1);//levágjuk az utolsó space karaktert
            ////s = s.Trim(); //ez levágja az üres karaktereket a string két végéről
            //Console.WriteLine(s);


            //VAGY stringbuilder, ez hatékonyabb
            //ha háromnál többször akarsz stringet módosítani, akkor használj stringbuildert, ökörszabály
            //StringBuilder sb = new StringBuilder(30);//megadhatjuk előre, elsőre mennyi helyet foglaljon le, és amíg nenél nem lesz nagyobb addig nem lesz fölösleges stringmásolgatás
            //foreach (string seged in tomb)
            //{
            //    sb.AppendFormat("{0} ", seged);
            //}
            //sb.Remove(sb.Length - 1, 1);//levesszük az utolsó spacet
            //Console.WriteLine(sb.ToString());
            //toString nél ha voltak még üreskarakterek a stringbuilderben akkor azokat nem rakja bele
            //van saját tartalomürítése

            StringBuilder builder = new StringBuilder("alma");
            Console.WriteLine(builder);
            WriteBeka(ref builder);
            Console.WriteLine(builder);

            string s = "szederkörte";
            int i;
            if(int.TryParse(s, out i))//ez egy true lesz ha sikerül, és i-be beleírja as s-t, ha nem sikerül akkor is ír valamit i-be
                Console.WriteLine(i);

            Console.ReadLine();
        }

        private static void WriteBeka(ref StringBuilder sb)
        {
            //sb.Clear();//törlöm a tartalmát
            sb = new StringBuilder();//ha nem ref lenne átadáskor, akkor ez a változtatás elveszne, reffel nem veszik el
            sb.Append("béka");//ezzel rakunk valamit a stringbuilderbe
        }
    }
}
