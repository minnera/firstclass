﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
//most nem a http filteres kell, hanem az mvc-s!!!!

namespace WebLayer.Classes
{
    public class VisitCounterFilterAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            object obj = filterContext.HttpContext.Session["VisitCount"];
            if (obj == null)
            {
                obj = 1;
            }
            else
            {
                obj = (int) obj + 1;
            }
            filterContext.HttpContext.Session["VisitCount"] = obj;
            //az a biztosabb ha visszaírjuk az eredményt
        }
    }
}