﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using WebLayer.Models;

namespace WebLayer.Controllers
{
    public class FormController : Controller
    {
        // GET: Form
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ProcessOne()
        {
            ViewBag.ProcessedOutput = String.Format("PROCESS ONE: uname = {0}, upass = {1}", Request["userName"],
                Request["userPass"]);
            return View("Index");
        }
        [HttpPost]
        public ActionResult ProcessTwo(string userName, string userPass)
        {
            ViewBag.ProcessedOutput = String.Format("PROCESS TWO: uname = {0}, upass = {1}", userName,
                userPass);
            return View("Index");
        }

        public ActionResult Ajax()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Calculate(double amount, double rate, int year)
        {
            double total = (amount * rate * year)/100;
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("<strong>Amount:</strong> {0}\n", amount);
            sb.AppendFormat("<strong>Rate:</strong> {0}\n", rate);
            sb.AppendFormat("<strong>Years:</strong> {0}\n", year);
            sb.AppendFormat("<strong>Total interest:</strong> {0}\n", total);
            ViewBag.FormResults = sb.ToString();

            //return View("Ajax"); //with BeginForm
            //ha ajaxom lesz:
            return Content(sb.ToString()); //with AjaxForm
        }

        public ActionResult AjaxModel()
        {
            CalculationModel model = new CalculationModel();
            model.Amount = 10000;
            model.Rate = 4.2;
            model.Years = 10;
            return View(model);
        }
        [HttpPost]
        //sql injection(ez ellen véd az entity framework ugye), XSS/Script injection(ez ellen véd a razer, a template engine),
        //CSRF = cross site request forgery: az űrlapfeldolgozómba nem az én űrlapomtól jön adat, hanem eviljoetól
        //ez ellen: automata formrajzoló véd
        //ehhez nekem ezt alattább jeleznem kell:
        [ValidateAntiForgeryToken] 
        public ActionResult CalculateWithModel(CalculationModel model)
        {
            //megfelelő e a megkapott adat?
            if (ModelState.IsValid)
            {
                double total = (model.Amount * model.Rate * model.Years) / 100;
                StringBuilder sb = new StringBuilder();
                sb.AppendFormat("<strong>Amount:</strong> {0}\n", model.Amount);
                sb.AppendFormat("<strong>Rate:</strong> {0}\n", model.Rate);
                sb.AppendFormat("<strong>Years:</strong> {0}\n", model.Years);
                sb.AppendFormat("<strong>Total interest:</strong> {0}\n", total);
                ViewBag.FormResults = sb.ToString();

                return Content(sb.ToString());
            }
            else
            {
                return Content("INVALID DATA");
            }
        }
    }
}