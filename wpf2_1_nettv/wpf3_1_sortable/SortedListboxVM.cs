﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace wpf3_1_sortable
{
    class SortedListboxVM : Bindable
    {
        private bool pointsDown;
        private IEnumerable<IComparable> listItems;

        public bool PointsDown
        {
            get { return pointsDown; }
            set { SetValue(ref pointsDown, value); }
        }

        public IEnumerable<IComparable> ListItems
        {
            get { return listItems; }
            set { SetValue(ref listItems, value); }
        }
    }
}
