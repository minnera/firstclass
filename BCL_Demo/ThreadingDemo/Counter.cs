﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ThreadingDemo
{
    class Counter
    {
        private static readonly object _locker = new object();
        private static int _count;
        private static object interlocked;

        public static int Count
        {
            get
            {
                return _count;
            }

            set
            {
                _count = value;
            }
        }

        public static void Increment()
        {
            for(int i = 0; i < 1000000; i++)
            {
                //csak referenciás objektumot tud lockolni.
                //lock (_locker)
                //{
                //    _count++; 
                //}
                //egy jobb megoldás:
                Interlocked.Increment(ref _count);

                //ha integerrel dolgozol nem feltétlenül saját locker kell, hanem Interlocked is lehet.
            }
        }
    }
}
