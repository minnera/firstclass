//------------------------------------------------------------------------------
// <copyright file="WebDataService.svc.cs" company="Microsoft">
//     Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Data.Services;
using System.Data.Services.Common;
using System.Data.Services.Providers;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Web;
using Data;

namespace WebLayer
{
    //nuget console, selected project: WebLAyer
    //install-package Microsoft.OData.EntityFrameworkProvider -Pre
    [ServiceBehavior(IncludeExceptionDetailInFaults = true)]
    public class EmpService : EntityFrameworkDataService<EmpDeptEntities>
    {
        // This method is called only once to initialize service-wide policies.
        public static void InitializeService(DataServiceConfiguration config)
        {
            // TODO: set rules to indicate which entity sets and service operations are visible, updatable, etc.
            // Examples:
            // config.SetEntitySetAccessRule("MyEntityset", EntitySetRights.AllRead);
            // config.SetServiceOperationAccessRule("MyServiceOperation", ServiceOperationRights.All);
            config.DataServiceBehavior.MaxProtocolVersion = DataServiceProtocolVersion.V3;

            //b�rmelyik entit�shoz b�rki hozz�f�rhet�, korl�toz�s n�lk�l:
            config.SetEntitySetAccessRule("*", EntitySetRights.All);


            config.SetServiceOperationAccessRule("EmpByName", ServiceOperationRights.All);

            config.UseVerboseErrors = true;
        }

        //ezzel jelezz�k hogy ez sz�pen megh�vhat� getre
        [WebGet]
        public IQueryable<EMP> EmpByName(string name)
        {
            if (name == null) return null;
            return from akt in CurrentDataSource.EMP
                where akt.ENAME.ToLower().Contains(name.ToLower())
                select akt;
        }
    }
}
