﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExceptionDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                int age = GetAge();
                if (age < 18)
                {
                    Console.WriteLine("Gyerek vagy!");
                }
                else
                {
                    Console.WriteLine("Nagyra nőtt gyerek vagy!");
                }
            }
            /*catch (FormatException)
            {
                Console.WriteLine("Ez nem életkor volt.");
            }
            catch (OverflowException)
            {
                Console.WriteLine("Túl nagy érték.");
            }*/
            catch(AgeException aex)
            {
                //Console.WriteLine(aex.InnerException.Message);
                Console.WriteLine("Hiba");
            }
            /*catch(Exception){//ez minden hibát elkap, nem illik használni
                Console.WriteLine("te hibád.");
                return;//ezzel leáll a program. de előtte a finally lefut.
            }
            */
            Console.ReadLine();
        }
        /// <summary>
        /// Bekéri és ellenőrzi az életkort.
        /// </summary>
        /// <exception cref=""></exception>
        /// <returns></returns>
        private static int GetAge()
        {
            Console.WriteLine("Add meg a korod:");
            string s = Console.ReadLine();
            try
            {
                int i = int.Parse(s);//most csak ebben a try blokkban létezik az i
                if(i < 0 || i > 130)//nem mindig kell exceptionnel hibát keresni, így is lehet pl.
                {
                    throw new AgeException();
                }
                return i;
                //file megnyitása
                //file használata
                //file bezárása, de nem, azt nem szabad itt hagyni, hanem finallyba.
            }
            catch (FormatException fex)//megnézzük, hogyan tudjuk megnézni, mit kaptunk, it nem int volt a stringben
            {
                //Console.WriteLine(fex.Message);//így elkaptuk és elnyeltük és igazán nem csináltunk vele semmit, ezért kell a throw
                //return -1;
                AgeException aex = new AgeException("Nem megfelelő érték", fex);
                //aex.InnerException = fex;
                throw aex; //ekkor az elkapott kivételt tovább is dobjuk felfele a hierarchiában, így a hívó is megkapja a FE-t
                //most saját kivételt dobunk így már.
            }
            catch (OverflowException ov)//ez akkor van, ha túl nagy a szám
            {
                //Console.WriteLine(ov.Message);
                AgeException aex = new AgeException("Nem megfelelő érték", ov);
                //aex.InnerException = ov;
                throw aex;
            }
            finally
            {
                Console.WriteLine("GetAge vége");
                //file bezárása
            }
            //return int.Parse(Console.ReadLine());
        }
    }
}
