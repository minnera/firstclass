﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CS3Demo
{
    class Program
    {
        static void Main(string[] args)
        {
            //Init();

            //AnonymousType();

            //Lambda();

            Person p = new Person() { Age = 8, Name = "Stan" };
            bool calidAge = p.Age.IsValidAge();
            
            Console.ReadLine();
        }

        private static void Lambda()
        {
            Func<int, int, string> f1 = delegate (int i, int j) 
            {
                return (i + j).ToString();
            };
            // nem is kéne oda irni azt hogy inti, int j, csak hogy i, j, mert nem lehet más.
            Func<int, int, string> f2 = ( i,  j) =>
            {
                return (i + j).ToString();
            };
            //mikor csak egy utasítás van, elég ennyi:
            Func<int, int, string> f3 = (i, j) => (i + j).ToString();
            string s2 = f3(1, 2);
            //iszonyatosan nagy előnye hogy nem kell kiírni a típusokat, majd linq-nél látjuk
            var ppl = new List<Person>() {
                new Person() { Name = "Stan", Age = 8, City = "South Park"}, new Person() { Name = "Aron", Age = 10, City = "North Park"}, new Person() { Name = "Sem", Age = 2, City = "South Park"}
            };

            var emberek = ppl.Select(p => new { p.Name, p.Age }).Where(
                x => x.Age > 7);
        }

        private static void AnonymousType()
        {
            List<Person> ppl = new List<Person>() {
                new Person() { Name = "Stan", Age = 8, City = "South Park"}, new Person() { Name = "Aron", Age = 10, City = "North Park"}, new Person() { Name = "Sem", Age = 2, City = "South Park"}
            };

            //List<Person> spk = new List<Person>();

            //foreach (Person p in ppl)
            //{
            //    if (p.City == "South Park")
            //    {
            //        spk.Add(p);
            //    }
            //}


            //ilyet is lehet, nevét se kell megadni, vag yát lehet nevezni
            object o = new {ppl[0].Name, Kor = ppl[0].Age };
            //de erre nem tudunk semmi függvényt meghívni, meg castolni, mert nincs neve a típusnak, és objectként van kezelve.
            //ezért lehet mondjuk var-t használni
            //itt a jobb oldal alapján ad típust neki a complájner
            var v = new { ppl[0].Name, Kor = ppl[0].Age };
            //lesz így iylen.
            string s = v.Name;
            //bármit helyettesíthetünk var-ral, és ő kitalálja.
            //csak lokális változónál használható, osztály tagja nem lehet, sem visszatérési érték, stb.


        }

        static void Init()
        {
            int[] intek = { 1, 2, 3 };

            List<Person> ppl = new List<Person>() {
                new Person() { Name = "Stan", Age = 8, City = "South Park"}, new Person() { Name = "Aron", Age = 10, City = "North Park"}, new Person() { Name = "Sem", Age = 2, City = "South Park"}
            };

            Dictionary<string, int> dict = new Dictionary<string, int>()
            {
                { "alma", 0},
                { "körte", 10},
                { "Tripla", -22}
            };

        }
    }
}
