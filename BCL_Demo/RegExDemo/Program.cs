﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;//ez kell a használatához

namespace RegExDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            ///###
            ///##-###
            ///###-###
            ///ezt a három formátum lehet, bármelyik, de valamelyik
            Console.WriteLine("Adj meg egy MCP viszgakódot: ");
            string input = Console.ReadLine();

            string regex1 = @"^(\d{2,3}-)?\d{3}$";
            string regex2 = @"^\d{2}((\d)|(\d-\d{3})|(-\d{3}))$";
            string regex3 = @"^\d{3}|(\d{2}-\d{3})|(\d{3}-\d{3})$";

            //ékezetes karakterek

            string regexplus = @"[a-zA-ZÉÁŐÚŰÖÜÓéáőúűóüö]";

            if(Regex.IsMatch(input, regex1))
            {
                Console.WriteLine("Megfelelő formátumú");

                //keressük az utolsó három karaktert
                string regexharom = @"^(\d{2,3}-)?(?<lastnumbers>\d{3})$";
                Regex rx = new Regex(regexharom);
                Match m = rx.Match(input);
                Console.WriteLine(m.Groups["lastnumbers"]);//ezzel ki lehet olvasni a regkifejezésben létrehozott csoportokat
                Console.WriteLine(m.Groups[2]);//a 0-adik az a egész stringet fogja mindig visszaadni , az első csoport az 1-es.
                Console.WriteLine(m.Result("${lastnumbers}"));
            }
            else
            {
                Console.WriteLine("Nem megfelelő formátumú");
            }

            Console.ReadLine();
        }
    }
}
