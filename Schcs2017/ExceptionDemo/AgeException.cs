﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExceptionDemo
{
    class AgeException : Exception
    {
        public AgeException(string message) : base(message)//itt megmondjuk az ős melyik construktorját hívjuk meg
        {

            //this.Message = message;//ezt az őskonstruktor nem engedi
        }

        public AgeException(string message, Exception inner) : base(message, inner)
        {

        }

        public AgeException() : base("Nem megfelelő érték.")//ha paraméter nélkül csinálunk valamit, autó ezt fogja neki adni
        {

        }

    }
}
