﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventDemo
{
    class Alien
    {
        
        public void Spy(object cm, PulseChangedEventArgs p)
        {
            Console.WriteLine(((Human)cm).Name + " pulzusa most : " + ((Human)cm).Pulse + " az előző pulzusa : " + p.Previous);
        }
    }
}
