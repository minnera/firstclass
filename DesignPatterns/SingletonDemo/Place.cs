﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SingletonDemo
{
    abstract class Place
    {

        public abstract int Id { get; }
        public abstract void Greet();

    }

    class Dungeon : Place
    {
        private int _id;
        private static int _nextId;

        public override int Id
        {
            get { return _id; }
        }

        public Dungeon()
        {
            _id = _nextId++;
        }
        public override void Greet()
        {
            Console.WriteLine("Te a {0} számú barlangba léptél be.", Id);
        }
    }

    class Town : Place
    {
        private readonly int _id = -1;
        public override int Id { get { return _id; } }

        private static Town _instance = new Town();
        //ha soká tart létrehozni, és ritka hoyg szükség van rá, akkor nem a mezőben hozzuk létre, hanem a get-ben, megnézzük null-e és ha igen, létrehozzuk
        //figyelni kell a get-ben létrehozásra, nem szálbiztos alapból
        //lockolni szoktuk
        //néha: null ellenőrzés, lockolás, null ellenőrzés megint és utána létrehozás
        public static Town Instance
        {
            get { return _instance; }
        }

        private Town()
        {
            
        }


        public override void Greet()
        {
            Console.WriteLine("Beléptél a városba. Ellopták a pénzedet.");
        }
    }
}
