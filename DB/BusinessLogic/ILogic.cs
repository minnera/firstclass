﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data;

namespace BusinessLogic
{
    public interface ILogic
    {
        IQueryable<GetAveragesResult> GetAverages();
        IQueryable<EMP> GetWorkers();
        IQueryable<DEPT> GetDepartments();

        //kb kapuként szolgál, milyen függvényeket engedek meg, hogy kívülről elérhetők legyenek.
        EMP GetOneWorker(int id);
        void AddDept(DEPT newdept);

        //törlése részlegnek
        void DelDept(int id);

        //a megadott részleg nevét és loc-ját módosítja át
        void ModifyDept(int id, string name, string loc);
        //egy darab részleg lekérése
        DEPT GetOneDept(int id);

        //
        int GetNextDeptno();
    }
}
