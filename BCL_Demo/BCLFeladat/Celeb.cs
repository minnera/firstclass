﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCLFeladat
{
    [Serializable]
    public class Celeb
    {
        static Random rnd = new Random();
        protected string _nev;
        protected int _iq = rnd.Next(0,101);

        public string Nev
        {
            get
            {
                return _nev;
            }
            set
            {
                _nev = value;
            }

        }

        public int Iq
        {
            get
            {
                return _iq;
            }

            set
            {
                _iq = value;
            }
        }

        public Celeb()
        {
            _nev = "Senki";
        }

        public Celeb(string name)
        {
            _nev = name;
        }
        public Celeb(string name, int iq)
        {
            Iq = iq;
            _nev = name;
        }
    }
}
