﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MutexDemo
{
    class Program
    {
        const string MUTEXNAME = "MYMUTEX";
        static void Main(string[] args)
        {
            Mutex m = null;
            //nézi hogy fut e már a programunk.
            try
            {
                m = Mutex.OpenExisting(MUTEXNAME);
                Console.WriteLine("Ez a program már fut");
                Thread.Sleep(3000);
                return;
            }
            catch (WaitHandleCannotBeOpenedException)
            {
                m = new Mutex(true, MUTEXNAME);
                Console.WriteLine("Mutex létrehozva");
            }

            Console.ReadLine();
        }
    }
}
