﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Munkahely
{
    class Munka
    {
        private string _leiras;

        public string Leiras
        {
            get
            {
                return _leiras;
            }

            set
            {
                _leiras = value;
            }
        }

        public Munka(Melosok m)
        {
            _munkaJellege = m.ToString();
        }

        public string MunkaJellege
        {
            get
            {
                return _munkaJellege;
            }

            set
            {
                _munkaJellege = value;
            }
        }

        private string _munkaJellege;
    }
}
