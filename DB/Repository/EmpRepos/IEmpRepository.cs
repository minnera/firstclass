﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data;
using Repository.GenericRepos;

namespace Repository.EmpRepos
{
    public interface IEmpRepository : IRepository<EMP>
    {
        //mivel nem kell extra ezért ezt üresen hagyjuk
    }
}
