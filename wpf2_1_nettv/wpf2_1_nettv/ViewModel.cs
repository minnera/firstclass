﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace wpf2_1_nettv
{
    class ViewModel : Bindable
    {
        private Order currentOrder;


        public Order CurrentOrder
        {
            get { return currentOrder; }
            set { SetValue(ref currentOrder, value); }
        }

        //most tudom hogy a lista soha nem fog változni
        public Array RatingValues
        {
            get { return Enum.GetValues(typeof (RatingType)); }
        }
    }
}