﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Munkahely
{
    abstract class Melos
    {
        private Melosok[] _kepzettseg;

        private string _neve = "Béla";

        internal Melosok[] Kepzettseg
        {
            get
            {
                return _kepzettseg;
            }

            set
            {
                _kepzettseg = value;
            }
        }

        public string Neve
        {
            get
            {
                return _neve;
            }

            set
            {
                _neve = value;
            }
        }

        public Melos()
        {
            _kepzettseg = new Melosok[3];
        }

        public Melos(int i, string n)
        {
            _neve = n;
            _kepzettseg = new Melosok[3];
            if (i < 4 || i > 0)
            {
                for (int j = 0; j < i; j++)
                {
                    _kepzettseg[j] = (Melosok)j;
                }
            }
            else
            {
                Console.WriteLine("Max 3 képessége lehet a munkásnak, most 0 képzettsége van.");
            }
        }
    }
}
