﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SemaphoreDemo
{
    class Program
    {
        private static Semaphore semaphore = new Semaphore(2, 2);
        static void Main(string[] args)
        {
            //amikor létrejön megadjuk hány slot van benne, ennél nem lehet tulpakolni
            //ezzel korlátozhatjuk, hogy egyszerre hány szál futhat
            Thread[] ts = new Thread[10];

            for(int i = 0; i < ts.Length; i++)
            {
                ts[i] = new Thread(DoWork);
                ts[i].Start();

            }
            foreach(Thread t in ts)
            {
                t.Join();
            }

            Console.WriteLine("Main vége");

            Console.ReadLine();
        }

        private static void DoWork()
        {
            Console.WriteLine("Elindult az adott szál : " + Thread.CurrentThread.ManagedThreadId);
            semaphore.WaitOne();
            for(int i = 0; i < 10; i++)
            {
                Console.WriteLine(Thread.CurrentThread.ManagedThreadId);
                Thread.Sleep(500);
            }
            semaphore.Release(1);
            Console.WriteLine("Leállt: " + Thread.CurrentThread.ManagedThreadId);
        }
    }
}
