﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace LinqToXMLDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            //az xml olvasó csak előrefelé tud olvasni, ez egy hátránya

            //lettek csomópont átrendezők, doc

            //X csilalg típusok, pl :
            //XElement root = new XElement("products", "alma");//az első a megnevezés, a második a tartalom
            ////<products>alma</products>

            //Console.WriteLine(root);

            ////lehet többet is egyszerre létrehozni

            //XElement roots = new XElement("prodicts",
            //    new XElement("products"),
            //    new XElement("products"));

            //Console.WriteLine(roots);

            //XElement roots2 = new XElement("prodicts",
            //    new XElement("products",
            //        new XAttribute("id", 0),
            //        new XElement("name", "bor"),
            //        new XElement("price", 200)),
            //    new XElement("products",
            //        new XAttribute("id", 1),
            //        new XElement("name", "palinka"),
            //        new XElement("price", 1500)),
            //    new XElement("products",
            //        new XAttribute("id", 2),
            //        new XElement("name", "sor"),
            //        new XElement("price", 200)));

            //Console.WriteLine(roots2);

            //XDocument xdoc = new XDocument(roots2);
            //xdoc.Save("products.xml"); //ez létrehoz egy xml fájlt.


            //betöltése

            XDocument doc = XDocument.Load("products.xml");
            //Console.WriteLine(doc);

            //tudunk rajta lekérdezéseket hívni, miután betöltöttük fájlból
            //mindig ki kell írni az element elé hogy mi az!!! (int) pl.

            var items = from e in doc.Descendants("products")
                        where (int)e.Element("price") > 400
                        orderby (string)e.Element("name")
                        select new { Id = (int)e.Attribute("id"), Name = (string)e.Element("name") };

            foreach(var item in items)
            {
                Console.WriteLine(item);
            }

            Console.ReadLine();
        }
    }
}
