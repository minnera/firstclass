﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;//ez kell

using System.Text;
using System.Threading.Tasks;

namespace BinarySerializationDemo
{
    [Serializable]
    class Person : IDeserializationCallback
    {
        private string _name;

        public string Name
        {
            get
            {
                return _name;
            }

            set
            {
                _name = value;
            }
        }

        private DateTime _birtdate;

        public DateTime Birtdate
        {
            get
            {
                return _birtdate;
            }

            set
            {
                _birtdate = value;
            }
        }
        [NonSerialized]
        private int _age;

        public int Age
        {
            get
            {
                return _age;
            }

            set
            {
                _age = value;
            }
        }

        public Person(string name, DateTime birth, int age)
        {
            _name = name;
            _birtdate = birth;
            _age = age;
        }

        public void OnDeserialization(object sender)
        {
            _age = (int)(((DateTime.Now - _birtdate).TotalDays)/365.25);
        }
    }
}
