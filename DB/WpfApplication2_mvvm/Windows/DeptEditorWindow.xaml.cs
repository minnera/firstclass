﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using GalaSoft.MvvmLight.Messaging;
using Microsoft.Practices.ServiceLocation;
using WpfApplication2_mvvm.Models;
using WpfApplication2_mvvm.Services;
using WpfApplication2_mvvm.ViewModel;

namespace WpfApplication2_mvvm.Windows
{
    /// <summary>
    /// Interaction logic for DeptEditorWindow.xaml
    /// </summary>
    public partial class DeptEditorWindow : Window
    {
        public DeptEditorWindow(DeptVM d = null)
        {
            InitializeComponent();

            ServiceLocator.Current.GetInstance<IDeptEditorViewModel>().EditedDept = d;
        }

        private void DeptEditorWindow_OnLoaded(object sender, RoutedEventArgs e)
        {
            //nagyon fontos hogy ha egyszer feliratkozom a registerrel egy eseményre valamikor le is kell arról iratkoznom majd!
            Messenger.Default.Register<bool>(this, ProcessResult);//(Target, Token, Handler); , a token ha sok féle üzenet van ez adja meg hogy milyen kell nekünk, pl "valami"
            //SomeObject.Event += ProcessResult;
        }

        private void ProcessResult(bool msg)
        {
            DialogResult = msg; //true - ok, false - cancel
            Close();
            //vissza is kéne jelezni az ablaknak, hogy most melyik gombra kattintottam
        }

        private void DeptEditorWindow_OnClosing(object sender, CancelEventArgs e)
        {
            Messenger.Default.Unregister(this); //így minden üzenet típus amire ez az ablak feliratkozott, azokról mind leiratkozunk így
        }
    }
}
