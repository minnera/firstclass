﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using AutoMapper;
using BusinessLogic;
using GalaSoft.MvvmLight.Ioc;
using Microsoft.Practices.ServiceLocation;
using WpfApplication2_mvvm.Models;
using WpfApplication2_mvvm.Services;
using WpfApplication2_mvvm.ViewModel;

namespace WpfApplication2_mvvm
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public App()
        {
            //service locator pattern = inversion of control
            ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);
            //összekötések
            SimpleIoc.Default.Register<IMainViewModel, MainViewModel>();
            SimpleIoc.Default.Register<IMainViewLogic, MainViewLogic>();
            SimpleIoc.Default.Register<IDeptEditorViewModel, DeptEditorViewModel>();
            SimpleIoc.Default.Register<IDeptEditorService, DeptEditorService>();

            //ez nem működne jól, amíg a reallogicnak 2 konstruktora van, esetleg [preferredconstructor] jelzővel. de a bl részben ne legyen olyan dolog ami az mvvm mel kapcsoalatos
            //SimpleIoc.Default.Register<ILogic, RealLogic>();
            //így fog működni
            SimpleIoc.Default.Register<ILogic>(() => new RealLogic());

            SimpleIoc.Default.Register<IMapper>(() => AutoMapperConfig.GetMapper());

            //bárhol máshol a programban így fogom elérni a típusokat
            //ServiceLocator.Current.GetInstance<ILogic>();
        }
    }
}
