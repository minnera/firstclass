﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.SignalR.Client;
using Newtonsoft.Json;

namespace SignalR_Client
{
    class Dept
    {
        public int deptno { get; set; }
        public string dname { get; set; }
        public string loc { get; set; }

        public override string ToString()
        {
            return String.Format("[{0}] {1} at {2}", deptno, dname, loc);
        }
    }
    class Program
    {
        //18758
        static string host = "http://localhost:8080/";

        static void AllDepts_Webapi()
        {
            Console.Clear();
            string url = host + "api/test/depts";
            string json = new WebClient().DownloadString(url);
            var list = JsonConvert.DeserializeObject<List<Dept>>(json);
            foreach (var item in list)
            {
                Console.WriteLine(item.ToString());
            }
        }

        static async void AllDepts_Signalr(IHubProxy hubProxy)
        {
            Console.Clear();
            Console.WriteLine("Fetching...");
            //DEADLOCK
            //var list = hubProxy.Invoke<IEnumerable<Dept>>("GetDepts").Result;
            var list = await hubProxy.Invoke<IEnumerable<Dept>>("GetDepts");
            foreach (var item in list)
            {
                Console.WriteLine(item.ToString());
            }
        }

        static void Main(string[] args)
        {
            Console.Clear();
            Console.WriteLine("PRESS ENTER TO START (when the web api started)");
            Console.ReadLine();

            
            //Solution jobb katt, set startup projects
            //ServiceEF / WebLayer + SignalR_Client

            HubConnection hub = new HubConnection(host);
            var hubProxy = hub.CreateHubProxy("deptHub");

            hubProxy.On("refresh", () =>
            {
                AllDepts_Signalr(hubProxy);
            });

            hubProxy.On<Dept>("deptAdded", dept =>
            {
                Console.WriteLine("NEW DEPT: " + dept.ToString());
            });

            hub.Start()
                .ContinueWith(task =>
                {
                    if (task.IsFaulted)
                    {
                        Console.WriteLine(task.Exception.GetBaseException());
                    }
                    else
                    {
                        Console.WriteLine("CONNECTED");
                    }
                })
                .Wait();

            //AllDepts_Webapi();
            AllDepts_Signalr(hubProxy);
            

            Console.ReadLine();

            Console.WriteLine("ADDING....");
            hubProxy.Invoke("AddDept", new Dept() {deptno = 77, dname = "a", loc = "b"}).Wait();
            Console.ReadLine();
            //ez is fentebb async!!!

            Console.WriteLine("DELETING...");
            hubProxy.Invoke("DelDept", 77).Wait();
            Console.ReadLine();

            hub.Stop();
            Console.WriteLine("STOPPED");
            Console.ReadLine();
        }
    }
}
