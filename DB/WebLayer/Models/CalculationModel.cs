﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebLayer.Models
{
    public class CalculationModel
    {
        [Display(Name = "Initial Amount")]//név
        //reklamáljon ha nincsen kitöltve, ez kötelező mező
        [Required(ErrorMessage = "The amount field is obligatory")]
        public double Amount { get; set; }
        [Display(Name = "Yearly interest rate")]
        public double Rate { get; set; }
        [Display(Name = "Number of years")]
        [Range(minimum: 1, maximum:10, ErrorMessage = "Year must be between 1 and 10")]
        public int Years { get; set; }
    }
}