﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace wpf3_1_sortable
{
    /// <summary>
    /// Interaction logic for SortedListbox.xaml
    /// </summary>
    public partial class SortedListbox : UserControl
    {
        private SortedListboxVM VM;
        //ez lesz a kapu kb a sortedlistboxon
        public static readonly DependencyProperty MyItemsProperty = DependencyProperty.Register("MyItems", typeof(IEnumerable<IComparable>), typeof(SortedListbox));

        public IEnumerable<IComparable> MyItems
        {
            get { return (IEnumerable<IComparable>) GetValue(MyItemsProperty); }
            set { SetValue(MyItemsProperty, value);}
        }

        //ezt amugy illene a ctor alá rakni
        private void DoSort()
        {
            if (MyItems == null) return;
            if (VM.PointsDown) //ha lefele mutat, akkor:
            {
                VM.ListItems = MyItems.OrderByDescending(x => x).ToList();
            }
            else
            {
                VM.ListItems = MyItems.OrderBy(x => x).ToList();
            }
        }
    

        public SortedListbox()
        {
            InitializeComponent(); //mindig ez alá írunk!

            DependencyPropertyDescriptor.FromProperty(MyItemsProperty, typeof(SortedListbox)).AddValueChanged(this,
                (sender, args) =>
                {
                    DoSort();
                });
            VM = FindResource("VM") as SortedListboxVM;
        }

        private void SortedListbox_OnLoaded(object sender, RoutedEventArgs e)
        {
            DoSort();
        }

        private void UpDownControl_OnIsDownChanged(object sender, EventArgs e)
        {
            DoSort();
        }
    }
}
