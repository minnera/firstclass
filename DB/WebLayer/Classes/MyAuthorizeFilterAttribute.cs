﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebLayer.Classes
{
    public class MyAuthorizeFilterAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            object obj = filterContext.HttpContext.Session["UserID"];
            if (obj == null)
            {
                filterContext.Result = new ContentResult() {Content = "ERROR 403"};
            }
        }
    }
}