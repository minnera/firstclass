using System.Collections.ObjectModel;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using Microsoft.Practices.ServiceLocation;
using WpfApplication2_mvvm.Models;

namespace WpfApplication2_mvvm.ViewModel
{
    /// <summary>
    /// This class contains properties that the main View can data bind to.
    /// <para>
    /// Use the <strong>mvvminpc</strong> snippet to add bindable properties to this ViewModel.
    /// </para>
    /// <para>
    /// You can also use Blend to data bind with the tool's support.
    /// </para>
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    
    // IMainViewModel -t is ide�rjuk
    public class MainViewModel : ViewModelBase, IMainViewModel
    {
        private DeptVM selectedDept;
        private ObservableCollection<DeptVM> depts;

        public DeptVM SelectedDept
        {
            get
            {
                return selectedDept;
            }
            set
            {
                Set(ref selectedDept, value);
                
            }
        }

        public ObservableCollection<DeptVM> Depts
        {
            get
            {
                return depts;
                
            }
            set
            {
                Set(ref depts, value);
                
            }
        }

        public ICommand AddCommand { get; private set; }
        public ICommand RemoveCommand { get; private set; }
        public ICommand ModifyCommand { get; private set; }
        public ICommand ChangeEnabledCommand { get; private set; }
        public ICommand UpdateViaJsonCommand { get; private set; }

        IMainViewLogic ViewLogic
        {
            get { return ServiceLocator.Current.GetInstance<IMainViewLogic>(); }
        }

        public MainViewModel()
        {
            ///if(IsInDesingMode)
            AddCommand = new RelayCommand(ViewLogic.AddCmd);
            RemoveCommand = new RelayCommand<DeptVM>(ViewLogic.DelCmd);
            ModifyCommand = new RelayCommand<DeptVM>(ViewLogic.ModCmd);
            ChangeEnabledCommand = new RelayCommand<MouseEventArgs>(ViewLogic.ChangeEnabledCmd);
            UpdateViaJsonCommand = new RelayCommand(ViewLogic.UpdateViaJsonCmd);

            depts = ViewLogic.GetDeptList();
        }

    }
}