﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlcoholDemo
{
    class Beer : Alcohol
    {
        //ez vagy 0, 1, 2
        //0: világos
        //1: barna
        //3: kék
        //de ez macerás másnak, legyen string.
        //ez se egyrtelmű, kisnagybetű, stb, ezért kell enum
        //private string _colour;
        private BeerColour _colour;

        public override double Percent
        {
            get { return _percent; }
            set
            {
                if (value < 0 || value > 30)
                {
                    throw new ArgumentException();
                }
                else
                    _percent = value;
            }
        }

        public BeerColour Colour
        {
            get
            {
                return _colour;
            }
            set
            {
                _colour = value;
            }
        }
        //EZ nem fog mindig működni!!!  kell a virtual és override, hog yszép legyen.
        public override void Drink()
        {
            base.Drink();
            Console.WriteLine("A sört korsóból isszák.");
        }
    }
    enum BeerColour : byte
    {
        Világos, Barna, Kék
    }

    class FruitBeer : Beer
    {
        public override void Drink()
        {
            Console.WriteLine("A gyümölcsös sört dobozból isszák.");
        }

        public override string ToString()
        {
            return "Gyümölcsös sör.";
        }
    }
}
