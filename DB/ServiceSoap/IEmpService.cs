﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
//leíja hogy a szolgáltatás milyen
//STO = Data Transfer Object , ennek implementálásával kezdünk itt. Az egyetlen feladata, hogy adatot továbbítson
//a szolgáltatások között továbbít adatot
//a DTO nem sokban különbözik az entity osztálytól.
//DRY - dont repeat yourself, miért kell akkor ilyen is az entity mellett?
//Entity object > transfer object > veiw object. ezek külön területek, mindet más osztállyal oldjuk meg.
//keverednének atributomokkal, ha egy osztállyal oldanánk meg mind a 3at.
//ezek jobbak elkülönítve. jobban kezelhető lesz az alkalmazás.
namespace ServiceSoap
{
    [ServiceContract]
    public interface IEmpService
    {
        [OperationContract]
        DTO.Emp GetWorkerById(int id);

        [OperationContract]
        List<DTO.Average> GetWorkerAverages();
    }

    //ezt máshol (DTO) csináltuk meg.


    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    //[ServiceContract]
    //public interface IService1
    //{
    //    [OperationContract]
    //    string GetData(int value);

    //    [OperationContract]
    //    CompositeType GetDataUsingDataContract(CompositeType composite);

    //    // TODO: Add your service operations here
    //}

    // Use a data contract as illustrated in the sample below to add composite types to service operations.
    // You can add XSD files into the project. After building the project, you can directly use the data types defined there, with the namespace "ServiceSoap.ContractType".



    //[DataContract]
    //public class CompositeType
    //{
    //    bool boolValue = true;
    //    string stringValue = "Hello ";

    //    [DataMember]
    //    public bool BoolValue
    //    {
    //        get { return boolValue; }
    //        set { boolValue = value; }
    //    }

    //    [DataMember]
    //    public string StringValue
    //    {
    //        get { return stringValue; }
    //        set { stringValue = value; }
    //    }
    //}
}
