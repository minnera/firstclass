﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace wpf2_1_nettv
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private ViewModel VM;
        public MainWindow()
        {
            InitializeComponent();
        }


        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            VM = FindResource("VM") as ViewModel;
            VM.CurrentOrder = new Order()
            {
                Bank = "12345678-12345678-12345678",
                Name = "Helga",
                Months = 12,
                Date = DateTime.Now.AddDays(10)
            };
        }

        List<Order> orders = new List<Order>(); 
        private void SaveClick(object sender, RoutedEventArgs e)
        {
            MessageBox.Show(VM.CurrentOrder.ToString());
            orders.Add(VM.CurrentOrder.Clone());
            orders.Add(VM.CurrentOrder.Clone());
            orders.Add(VM.CurrentOrder.Clone());
            orders.Add(VM.CurrentOrder.Clone());
            orders.Add(VM.CurrentOrder.Clone());
            orders.Add(VM.CurrentOrder.Clone());

            ListWindow win = new ListWindow(orders);
            win.ShowDialog();
        }
    }
}