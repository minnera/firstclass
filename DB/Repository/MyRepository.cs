﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Repository.DeptRepos;
using Repository.EmpRepos;

namespace Repository
{
    public class MyRepository
    {
        //Dependency Injection : design pattern
        public IEmpRepository empRepo { get; private set; }
        public IDeptRepository deptRepo { get; private set; }

        public MyRepository(IEmpRepository er, IDeptRepository dr)
        {
            empRepo = er;
            deptRepo = dr;
        }
    }
}