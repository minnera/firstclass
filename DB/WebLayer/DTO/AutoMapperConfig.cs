﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;

namespace WebLayer.DTO
{
    class AutoMapperConfig
    {
        public static IMapper GetMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                //össze párosít két osztályt.
                
                cfg.CreateMap<Data.EMP, DTO.Emp>();
                cfg.CreateMap<BusinessLogic.GetAveragesResult, DTO.Average>()
                    .ForMember(dest => dest.AvgSal, opt => opt.MapFrom(src => src.Avg));
                //ha már az első elem (itt dest)  megvan, akkor már a többi elemet autocompletelni tudja, mert tudja melyik overloadet használod)
                cfg.CreateMap<Data.DEPT, DTO.Dept>().ReverseMap();
                cfg.CreateMap<Data.DEPT, Models.DeptModel>().ReverseMap();
            });
            //.ForMember(dest => dest.TeamName, opt => opt.Ignore())
            return config.CreateMapper();
        }
    }
}
//itt a hibák most nincsenek lekezelve!
//itt szokás a névterek kiíása