﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace wpf4_1_pizza
{
    class MainViewModel : Bindable, IMainViewModel
    {
        private string address;

        public string Address
        {
            get { return address; }
            set {  SetValue(ref address, value); }
        }

        public Pizza Pizza { get; private set; }
        public IEnumerable<PizzaExtra> Sizes { get; private set; }
        public ICommand SelectAll { get { return new SelectAllCommand(); } }

        public MainViewModel()
        {
            Pizza = new Pizza();
            Pizza.Toppings.Add(new PizzaExtra() { Name = "Tomato", Price = 200});
            Pizza.Toppings.Add(new PizzaExtra() { Name = "Cheese", Price = 200 });
            Pizza.Toppings.Add(new PizzaExtra() { Name = "Bacon", Price = 500 });
            Pizza.Toppings.Add(new PizzaExtra() { Name = "Ham", Price = 300 });
            Pizza.Toppings.Add(new PizzaExtra() { Name = "Corn", Price = 300 });
            Pizza.Toppings.Add(new PizzaExtra() { Name = "Ruccola", Price = 300 });
            Pizza.Toppings.Add(new PizzaExtra() { Name = "Bolognese", Price = 500 });
            Pizza.Toppings.Add(new PizzaExtra() { Name = "Pepper", Price = 300 });
            Pizza.Toppings.Add(new PizzaExtra() { Name = "Leaf", Price = 200 });

            List<PizzaExtra> sizesList = new List<PizzaExtra>();
            sizesList.Add(new PizzaExtra() {Name = "23 cm", Price = 800});
            sizesList.Add(new PizzaExtra() { Name = "32 cm", Price = 1500 });
            sizesList.Add(new PizzaExtra() { Name = "48 cm", Price = 2900 });

            Sizes = sizesList;
            
            Pizza.Size = sizesList[1];
        }
    }
}
