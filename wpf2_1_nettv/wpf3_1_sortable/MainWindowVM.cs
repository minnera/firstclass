﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace wpf3_1_sortable
{
    class MainWindowVM : Bindable
    {
        private bool looksDown;
        private List<int> numbers; 

        public bool LooksDown
        {
            get { return looksDown; }
            set { SetValue(ref looksDown, value); }
        }

        public IEnumerable<IComparable> Numbers
        {
            get { return numbers.Cast<IComparable>(); }
            set { SetValue(ref numbers, value.Cast<int>().ToList()); }
        }

        public MainWindowVM()
        {
            numbers = new List<int>() {42, 22, 69, 56, 11};
        }
    }
}
