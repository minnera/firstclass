﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebLayer.Models
{
    public class DeptModel
    {
        [Display(Name="Department ID")]
        [Required]//kötelező megadni!
        public int deptno { get; set; }
        [Display(Name = "Department Name")]
        [Required]
        [StringLength(14, MinimumLength = 5)]//max 14 karakter, min. 5
        public string dname { get; set; }
        [Display(Name = "Department Location")]
        [Required]

        //ez a szintakszis csak attributomnál használható! mert a minimumlenght az már tulajdonság, és nem a konstruktor fejlécének része
        [StringLength(20, MinimumLength = 5)]//max 20 karakter, min. 5
        public string loc { get; set; }
    }
}