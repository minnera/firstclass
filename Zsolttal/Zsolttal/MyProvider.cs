﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Security.AccessControl;
using System.Text;
using System.Threading.Tasks;

namespace Zsolttal
{
    //ő fogja csinálni a tényleges sql elérést

    //leszármazottak:
    //DbConnection > SqlConnection, OracleConnection, MySqlConnection, NpgsqlConnection
    //soha nem használjuk a beépített oracle connectiont!!!!!
    //és van beépített....
    //ODAC / ODP.NET , ezt kell a beépített helyett letölteni és használni
    //
    //ezek is abstract osztályok
    //DbCommand
    //DbDataReader
    //
    //ODBC : ez a teljesen gyártótól független elvileg, ez mindig lassabb viszont
    class MyProvider
    {
        //ez egy abstract dolog, ennek valami leszármazottját fogjuk ebbe rakni
        private DbConnection server;

        public MyProvider(string connStr)//connection string: key1 = value; key2 = value; .... kulcs-érték párok
        {
            server = new SqlConnection(connStr);
            server.Open();
        }

        //a biztonság mindig nagyon fontos
        //string sql = "SELECT TOP 10 news_id FROM news ORDER BY news_date desc";
        //
        //    vagy
        //
        //string sql = "SELECT * FROM news WHERE news_title LIKE '%" + searchWord + "%' ";
        //ilyennél tud nagoyn sérülékeny lenni, sql injection-nél
        //
        //hogy kerüljük el?
        //
        //string sql = " SELECT * FROM users WHERE uname = '"+username+"' AND upass = '"+upass+"'";
        //
        //pl ilyen történhet:
        //username => ' OR 1=1 OR ''='
        //
        // 1) Escape
        //
        // minden veszélyes karakter elé \-t rakok. pl ' elé.
        //
        // ez könnyen elfelejthető és nem is olyan gyors
        //
        // 2) Prepared statements
        //
        // úgy fogalmazom meg az utasítás, hogy az sql utasításban jelzem, hogy ide egy adat fog jönni kívűlről :
        // WHERE uname = @username AND ....
        //
        // így nem fog utasításként értelmeződni
        //
        // 3) ORM
        //
        // ez egy rendszer, az ORM = Object-Relational Mapping
        // ezzel nekünk már nem sql utasításokkal kommunikálnunk az adatbázissal.
        // manapság ez az alap
        // sebesség szempontból még mindig messze van a sima sql-től


        //itt 2-es módszer lesz.
        DbCommand InitCommand(string sql, Dictionary<string, string> sqlParams)
        {
            DbCommand cmd = new SqlCommand(sql, (SqlConnection)server);

            if (sqlParams != null)
            {
                foreach (var item in sqlParams)
                {
                    DbParameter p = cmd.CreateParameter();
                    p.ParameterName = item.Key;
                    p.Value = item.Value;
                    cmd.Parameters.Add(p);
                }
            }
            return cmd;
            //selectet kivéve minden utasítás egy számot ad vissza, hogy mennyivel sikerült a dolog
            //insert, delete, create, drop, stb
            //de van amikor nem egyértelmű az, hogy a visszaadott szám mit jelent
            //van hogy -1 a siker, stb.
        }

        //1, nem select végrehajtása:

        public int ExecNonSelect(string sql, Dictionary<string, string> sqlParams = null)
        {
            DbCommand cmd = InitCommand(sql, sqlParams);

            return cmd.ExecuteNonQuery();
        }

        //2, mikor egy dolgot ad vissza a select

        public object ExecScalar(string sql, Dictionary<string, string> sqlParams = null)
        {
            DbCommand cmd = InitCommand(sql, sqlParams);

            return cmd.ExecuteScalar();
        }

        //3, nagy select, táblázatot kapunk vissza végrehajtása

        public MyResult ExecSelect(string sql, Dictionary<string, string> sqlParams = null)
        {
            DbCommand cmd = InitCommand(sql, sqlParams);
            //a soronkénti, oszloponkénti feldolgozást segíti
            DbDataReader reader = cmd.ExecuteReader();

            MyResult output = new MyResult();
            string[] fields = null;
            object[] values = null;

            while (reader.Read())//amíg sikerül a következő sort beolvasni
            {
                //meg kell nézni, hogy ez a legelső sor e, mert az előtt nem tudjuk feltölteni a tömböket
                if (fields == null)
                {
                    fields = new string[reader.FieldCount]; //ez nem fog módosulni
                    values = new object[reader.FieldCount];
                    //ezért most egyszer feltöltjük és ennyi elég
                    for (int i = 0; i < reader.FieldCount; i++)
                    {
                        fields[i] = reader.GetName(i).ToUpper(); //minden típusra van egy getter, figyelni kell mit hívunk meg
                        //ezzel megkapjuk az i-edik oszlopnak a nevét.
                    }
                }
                for (int i = 0; i < reader.FieldCount; i++)
                {
                    values[i] = reader.GetValue(i); //ez általános, tipikusan nem használjuk
                }
                output.AddRow(fields, values);
            }
            if (reader != null && !reader.IsClosed)
            {
                reader.Close();
            }
            return output;
        }

        public string ExecGetName(int id)
        {
            SqlCommand cmd = new SqlCommand("GetName", (SqlConnection)server);

            cmd.CommandType = System.Data.CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@id", id);

            SqlParameter p = cmd.CreateParameter();

            p.SqlDbType = System.Data.SqlDbType.NVarChar;

            p.Size = 50;

            p.Direction = System.Data.ParameterDirection.Output;
            p.ParameterName = "@Name";
            cmd.Parameters.Add(p);

            int result = cmd.ExecuteNonQuery();

            return p.Value.ToString();
        }
    }
}

//NChar és NVarChar unicode hoz, ékezettároláshoz, a sima Char és VarChar nem tudja kezelni az ékezeteseket


//eljárás ad vissza értéket
//a fuknció nem ad vissza értéket

    //a függvény mssqlben lebutitott

