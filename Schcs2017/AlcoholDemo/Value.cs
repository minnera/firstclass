﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlcoholDemo
{
    struct Value : IIncrementable
    {
        private int _x;

        public int X
        {
            get { return _x; }
            set { _x = value; }
        }
        public Value(int x)
        {
            _x = x;
        }
        public void Increment()
        {
            _x++;
        }
    }

    class RefValue : IIncrementable
    {
        private int _x;

        public int X
        {
            get { return _x; }
            set { _x = value; }
        }
        public RefValue(int x)
        {
            _x = x;
        }
        public void Increment()
        {
            _x++;
        }
    }

    interface IIncrementable
    {
        void Increment();
    }
}
